<?php
/**
 * Exergie functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Exergie
 * @since   1.0
 */

/**
 * Load Autoloader
 */
require_once 'inc/class-exergie-autoloader.php';
/**
 * Instantiate it
 */
$exergie = Exergie::get_instance();