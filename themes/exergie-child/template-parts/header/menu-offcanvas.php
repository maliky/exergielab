<?php
/**
 * Template part for displaying the menu
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Exergie
 */

?>
<div class="offcanvas">
	<div class="offcanvas__content">

		<div class="exergie-menu-icon exergie-menu-icon--open">
			<div class="exergie-navicon"></div>
		</div>
		
		<?php
		wp_nav_menu( array(
			'menu'           => 'primary',
			'theme_location' => 'primary',
			'container'      => '',
			'menu_id'        => 'menu',
			'menu_class'     => 'exergie-menu',	
		) );
		?>
	</div>
</div>
