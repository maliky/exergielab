<?php
// je préfix les noms des fonctions par ea_
add_action( 'wp_enqueue_scripts', 'eapc_theme_enqueue_styles' );

// Ajout d'une zone dans l'entête pour les liens des réseaux sociaux
add_action( 'widgets_init', 'eapc_widgets_init' );

/* Fonctions pour charger les styles.css */
function eapc_theme_enqueue_styles() {
    
    $parent_style = 'portum-style'; 
    
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
                      get_stylesheet_directory_uri() . '/style.css',
                      array( $parent_style ),
                      wp_get_theme()->get('Version')
    );
}


/* Fonction pour ajouter une zone dans l'interface des widgets, c'est à dire une sidebar */
function eapc_widgets_init(){
    register_sidebar(
	array(
            'id'            => 'header-sidebar-social-1',
            'name'          => __( "[header] 2e menu" ),
            'description'   => __( 'Principalement pour y mettre un menu pointant vers des sites `socials`.' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
	)
    );
}
?>
