<?php
/**
 * The template for displaying footer part
 *
 * @package Exergie
 */

get_sidebar( 'footer' );
?>
</div>

<?php if ( get_theme_mod( 'exergie_enable_go_top', true ) ) : ?>
    <a id="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
<?php endif; ?>

<?php wp_footer();?>

</body>

</html>
