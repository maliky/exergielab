<?php
 get_header(); ?>
  <?php 
// the query

$the_query = new WP_Query(array('category_name' => 'Formations','numberposts' => 4,) ); ?>
 
<?php if ( $the_query->have_posts() ) : ?>
 
    <!-- pagination here -->
 
    <!-- the loop -->
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="article"> 
            <hr></hr>
            <a href="<?php echo get_permalink(); ?>" >
            <div class="col-md-3">     
                <?php the_post_thumbnail(); ?>
            </div>
                <h2><?php the_title(); ?></h2>
                 <p><?php the_excerpt();?></p>
            </a>
        </div>
    <?php endwhile; ?>
    
    <!-- end of the loop -->
 
    <!-- pagination here -->
 
    <?php wp_reset_postdata(); ?>
 
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php
get_footer();?>