<!-- wp:heading -->
<h2>Programme de Formation en Efficacité Énergétique et Énergie Renouvelable (<strong>PFEER</strong>)</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Bien qu’il ait été démontré à maintes reprises que l’énergie durable contribue à la compétitivité des entreprises et augmente leur productivité, ses actions et avantages ne sont pas suffisamment pris en compte dans les stratégies de réduction des coûts de production en entreprise. Un certain nombre d'obstacles subsistent encore au rang desquels figurent en bonne position <strong>.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><p><strong>l'accès limité au savoir-faire technique et l'aversion pour le risque</strong></p></blockquote>
<!-- /wp:quote -->

<!-- wp:image {"id":1101,"align":"center","width":486,"height":473} -->
<div class="wp-block-image"><figure class="aligncenter is-resized"><img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/diagram_energie_durable800.svg_.png" alt="" class="wp-image-1101" width="486" height="473"/></figure></div>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Afin de surmonter cet obstacle majeur et repositionner l’énergie durable au centre des stratégies de réduction des coûts de production, Exergie Afrique, en partenariat avec plusieurs groupements professionnels, propose un plan de renforcement des capacités qui offre aux managers et gestionnaires ainsi qu’aux équipes techniques les connaissances requises pour la gestion quotidienne de leur entreprise, mais aussi pour discuter avec les professionnels en énergie lors de l’implantation des mesures d’énergie durable. Dans la pratique, nos formations offrent les prérequis d’une bonne gestion durable de l’entreprise, à savoir&nbsp;: 
</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li> L’assurance que l’énergie consommée et facturée au meilleur prix&nbsp;;
</li><li> L’assurance que l’énergie payée est utilisée judicieusement&nbsp;;
</li><li> La prise en compte de l’amélioration du confort en milieu de travail&nbsp;;
</li><li> La réduction des risques de pannes inopportunes&nbsp;;
</li><li> Le prolongement de la durée de vie utile des installations&nbsp;;
</li><li> L’augmentation générale de la productivité de l’entreprise&nbsp;;
</li><li> La réalisation d’économies financières substantielles qui se perpétuent.
</li></ul>
<!-- /wp:list -->



