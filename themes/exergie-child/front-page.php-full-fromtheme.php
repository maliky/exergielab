<!DOCTYPE html>
<html lang="en-US">
    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Exergie Afrique &#8211; Maîtriser l&#039;ENERGIE pour un développement plus durable</title>
	<meta name='robots' content='noindex,follow' />
	<link rel='dns-prefetch' href='//s.w.org' />
	<link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Feed" href="http://localhost/exergieafrique.dev/?feed=rss2" />
	<link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Comments Feed" href="http://localhost/exergieafrique.dev/?feed=comments-rss2" />
	<script type="text/javascript">
	 window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"wpemoji":"http:\/\/localhost\/exergieafrique.dev\/wp-includes\/js\/wp-emoji.js?ver=5.2.1","twemoji":"http:\/\/localhost\/exergieafrique.dev\/wp-includes\/js\/twemoji.js?ver=5.2.1"}};
	 /**
	  * @output wp-includes/js/wp-emoji-loader.js
	  */
	 ( function( window, document, settings ) {
	     var src, ready, ii, tests;
	     // Create a canvas element for testing native browser support of emoji.
	     var canvas = document.createElement( 'canvas' );
	     var context = canvas.getContext && canvas.getContext( '2d' );
	     /**
	      * Checks if two sets of Emoji characters render the same visually.
	      *
	      * @since 4.9.0
	      *
	      * @private
	      *
	      * @param {number[]} set1 Set of Emoji character codes.
	      * @param {number[]} set2 Set of Emoji character codes.
	      *
	      * @return {boolean} True if the two sets render the same.
	      */
	     function emojiSetsRenderIdentically( set1, set2 ) {
		 var stringFromCharCode = String.fromCharCode;
		 // Cleanup from previous test.
		 context.clearRect( 0, 0, canvas.width, canvas.height );
		 context.fillText( stringFromCharCode.apply( this, set1 ), 0, 0 );
		 var rendered1 = canvas.toDataURL();
		 // Cleanup from previous test.
		 context.clearRect( 0, 0, canvas.width, canvas.height );
		 context.fillText( stringFromCharCode.apply( this, set2 ), 0, 0 );
		 var rendered2 = canvas.toDataURL();
		 return rendered1 === rendered2;
	     }
	     /**
	      * Detects if the browser supports rendering emoji or flag emoji.
	      *
	      * Flag emoji are a single glyph made of two characters, so some browsers
	      * (notably, Firefox OS X) don't support them.
	      *
	      * @since 4.2.0
	      *
	      * @private
	      *
	      * @param {string} type Whether to test for support of "flag" or "emoji".
	      *
	      * @return {boolean} True if the browser can render emoji, false if it cannot.
	      */
	     function browserSupportsEmoji( type ) {
		 var isIdentical;
		 if ( ! context || ! context.fillText ) {
		     return false;
		 }
		 /*
		  * Chrome on OS X added native emoji rendering in M41. Unfortunately,
		  * it doesn't work when the font is bolder than 500 weight. So, we
		  * check for bold rendering support to avoid invisible emoji in Chrome.
		  */
		 context.textBaseline = 'top';
		 context.font = '600 32px Arial';
		 switch ( type ) {
		     case 'flag':
			 /*
			  * Test for UN flag compatibility. This is the least supported of the letter locale flags,
			  * so gives us an easy test for full support.
			  *
			  * To test for support, we try to render it, and compare the rendering to how it would look if
			  * the browser doesn't render it correctly ([U] + [N]).
			  */
			 isIdentical = emojiSetsRenderIdentically(
			     [ 0xD83C, 0xDDFA, 0xD83C, 0xDDF3 ],
			     [ 0xD83C, 0xDDFA, 0x200B, 0xD83C, 0xDDF3 ]
			 );
			 if ( isIdentical ) {
			     return false;
			 }
			 /*
			  * Test for English flag compatibility. England is a country in the United Kingdom, it
			  * does not have a two letter locale code but rather an five letter sub-division code.
			  *
			  * To test for support, we try to render it, and compare the rendering to how it would look if
			  * the browser doesn't render it correctly (black flag emoji + [G] + [B] + [E] + [N] + [G]).
			  */
			 isIdentical = emojiSetsRenderIdentically(
			     [ 0xD83C, 0xDFF4, 0xDB40, 0xDC67, 0xDB40, 0xDC62, 0xDB40, 0xDC65, 0xDB40, 0xDC6E, 0xDB40, 0xDC67, 0xDB40, 0xDC7F ],
			     [ 0xD83C, 0xDFF4, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC62, 0x200B, 0xDB40, 0xDC65, 0x200B, 0xDB40, 0xDC6E, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC7F ]
			 );
			 return ! isIdentical;
		     case 'emoji':
			 /*
			  * Love is love.
			  *
			  * To test for Emoji 12 support, try to render a new emoji: men holding hands, with different skin
			  * tone modifiers.
			  *
			  * When updating this test for future Emoji releases, ensure that individual emoji that make up the
			  * sequence come from older emoji standards.
			  */
			 isIdentical = emojiSetsRenderIdentically(
			     [0xD83D, 0xDC68, 0xD83C, 0xDFFE, 0x200D, 0xD83E, 0xDD1D, 0x200D, 0xD83D, 0xDC68, 0xD83C, 0xDFFC],
			     [0xD83D, 0xDC68, 0xD83C, 0xDFFE, 0x200B, 0xD83E, 0xDD1D, 0x200B, 0xD83D, 0xDC68, 0xD83C, 0xDFFC]
			 );
			 return ! isIdentical;
		 }
		 return false;
	     }
	     /**
	      * Adds a script to the head of the document.
	      *
	      * @ignore
	      *
	      * @since 4.2.0
	      *
	      * @param {Object} src The url where the script is located.
	      * @return {void}
	      */
	     function addScript( src ) {
		 var script = document.createElement( 'script' );
		 script.src = src;
		 script.defer = script.type = 'text/javascript';
		 document.getElementsByTagName( 'head' )[0].appendChild( script );
	     }
	     tests = Array( 'flag', 'emoji' );
	     settings.supports = {
		 everything: true,
		 everythingExceptFlag: true
	     };
	     /*
	      * Tests the browser support for flag emojis and other emojis, and adjusts the
	      * support settings accordingly.
	      */
	     for( ii = 0; ii < tests.length; ii++ ) {
		 settings.supports[ tests[ ii ] ] = browserSupportsEmoji( tests[ ii ] );
		 settings.supports.everything = settings.supports.everything && settings.supports[ tests[ ii ] ];
		 if ( 'flag' !== tests[ ii ] ) {
		     settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && settings.supports[ tests[ ii ] ];
		 }
	     }
	     settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && ! settings.supports.flag;
	     // Sets DOMReady to false and assigns a ready function to settings.
	     settings.DOMReady = false;
	     settings.readyCallback = function() {
		 settings.DOMReady = true;
	     };
	     // When the browser can not render everything we need to load a polyfill.
	     if ( ! settings.supports.everything ) {
		 ready = function() {
		     settings.readyCallback();
		 };
		 /*
		  * Cross-browser version of adding a dom ready event.
		  */
		 if ( document.addEventListener ) {
		     document.addEventListener( 'DOMContentLoaded', ready, false );
		     window.addEventListener( 'load', ready, false );
		 } else {
		     window.attachEvent( 'onload', ready );
		     document.attachEvent( 'onreadystatechange', function() {
			 if ( 'complete' === document.readyState ) {
			     settings.readyCallback();
			 }
		     } );
		 }
		 src = settings.source || {};
		 if ( src.concatemoji ) {
		     addScript( src.concatemoji );
		 } else if ( src.wpemoji && src.twemoji ) {
		     addScript( src.twemoji );
		     addScript( src.wpemoji );
		 }
	     }
	 } )( window, document, window._wpemojiSettings );
	</script>
	<style type="text/css">
	 img.wp-smiley,
	 img.emoji {
	     display: inline !important;
	     border: none !important;
	     box-shadow: none !important;
	     height: 1em !important;
	     width: 1em !important;
	     margin: 0 .07em !important;
	     vertical-align: -0.1em !important;
	     background: none !important;
	     padding: 0 !important;
	 }
	</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/dist/block-library/style.css?ver=5.2.1' type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3' type='text/css' media='all' />
	<link rel='stylesheet' id='ditty-news-ticker-font-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/libs/fontastic/styles.css?ver=2.2.1' type='text/css' media='all' />
	<link rel='stylesheet' id='ditty-news-ticker-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/css/style.css?ver=1558354078' type='text/css' media='all' />
	<link rel='stylesheet' id='widgetopts-styles-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/widget-options/assets/css/widget-options.css' type='text/css' media='all' />
	<link rel='stylesheet' id='exergie-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/style.css?ver=5.2.1' type='text/css' media='all' />
	<link rel='stylesheet' id='font-awesome-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/font-awesome/font-awesome.css?ver=5.2.1' type='text/css' media='all' />
	<link rel='stylesheet' id='ion-icons-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/ionicons/ion.css?ver=5.2.1' type='text/css' media='all' />
	<link rel='stylesheet' id='exergie-main-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/css/style-exergie.css?ver=1.0.6' type='text/css' media='all' />
	<style id='exergie-main-inline-css' type='text/css'>
	 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
	</style>
	<link rel='stylesheet' id='exergie-style-overrides-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/css/overrides.css?ver=5.2.1' type='text/css' media='all' />
	<style id='exergie-style-overrides-inline-css' type='text/css'>
	 /**
	    - epsilon_general_separator -
	    #C95A2E - epsilon_accent_color - #0385d0
	    #2C4088 - epsilon_accent_color_second - #a1083a
	    - epsilon_text_separator -
	    011769 - epsilon_title_color - #1a171c
	    rgba(0, 0, 0, 1) - epsilon_text_color - #212529
	    #0A2175 - epsilon_link_color - #0385d0
	    #AC3100 - epsilon_link_hover_color - #a1083a
	    #6B7598 - epsilon_link_active_color - #333333
	    - epsilon_menu_separator -
	    rgba(249, 251, 252, 1) - epsilon_header_background - #151c1f
	    #FFFFFF - epsilon_header_background_sticky - rgba(255,255,255,.3)
	    #A2A2A2 - epsilon_header_background_border_bot - rgba(255,255,255,.1)
	    #EFEFEF - epsilon_dropdown_menu_background - #a1083a
	    #E2E2E2 - epsilon_dropdown_menu_hover_background - #940534
	    rgba(0, 0, 0, 1) - epsilon_menu_item_color - #ebebeb
	    #AC3100 - epsilon_menu_item_hover_color - #ffffff
	    #2C4088 - epsilon_menu_item_active_color - #0385d0
	    - epsilon_footer_separator -
	    #0377bb - epsilon_footer_contact_background - #0377bb
	    #75A200 - epsilon_footer_background - #192229
	    #6A9200 - epsilon_footer_sub_background - #000
	    #011769 - epsilon_footer_title_color - #ffffff
	    rgba(0, 0, 0, 1) - epsilon_footer_text_color - #a9afb1
	    #011769 - epsilon_footer_link_color - #a9afb1
	    #EFEFEF - epsilon_footer_link_hover_color - #ffffff
	    #A2A2A2 - epsilon_footer_link_active_color - #a9afb1
	  */
	 /* ==========================================================================
	    =Accent colors
	    ========================================================================== */
	 .text-accent-color {
	     color:  #C95A2E;
	     /* epsilon_accent_color */
	 }
	 .text-accent-color-2 {
	     color: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 /* Hero - Slider */
	 .section-slider .pager-slider li {
	     background: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 .section-slider .pager-slider li.active {
	     background: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 /* Portfolio */
	 .portfolio-grid-item .action .zoom,
	 .portfolio-grid-item .action .link {
	     background: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 .portfolio-grid-item .action .zoom:hover,
	 .portfolio-grid-item .action .link:hover {
	     background: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 /* Blog */
	 .title-area .page-cat-links a {
	     background-color: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 .blog-news-item .news-category strong {
	     background: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 .blog-news-item .news-category strong:hover {
	     background: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 .blog-news-item h4 a:hover,
	 .blog-news-item h4 a:focus {
	     color: #AC3100;
	 }
	 .blog-news-item h4 a:active {
	     color: #6B7598;
	 }
	 /* Pricing */
	 .section-pricing .pricing-item .plan sup,
	 .section-pricing .pricing-item .plan sub,
	 .section-pricing .pricing-item .plan strong,
	 .section-pricing .pricing-item ul span {
	     color: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 .section-pricing .pricing-item .details {
	     background-color: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 .section-pricing .pricing-item:hover .plan sup,
	 .section-pricing .pricing-item:hover .plan sub,
	 .section-pricing .pricing-item:hover .plan strong,
	 .section-pricing .pricing-item:hover ul span {
	     color: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 .section-pricing .pricing-item:hover .details {
	     background-color: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 @media (max-width: 768px) {
	     .section-pricing .pricing-item .plan sup,
	     .section-pricing .pricing-item .plan sub,
	     .section-pricing .pricing-item .plan strong,
	     .section-pricing .pricing-item:hover .plan sup,
	     .section-pricing .pricing-item:hover .plan sub,
	     .section-pricing .pricing-item:hover .plan strong {
		 color: #fff;
	     }
	 }
	 /* Counters */
	 .ewf-counter__odometer,
	 .ewf-counter__symbol {
	     color: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 /* Team members */
	 .team-members-item:hover .overlay {
	     background: #C95A2E;
	     /* epsilon_accent_color */
	     opacity: .65;
	 }
	 /* Progress */
	 /*
	    .ewf-progress__bar-liniar-wrap {
	    background-color: #2C4088;
	    }
	  */
	 .ewf-progress__bar-liniar {
	     background-color: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 .ewf-progress--alternative-modern .ewf-progress__bar-liniar-wrap:after {
	     background: #2C4088;
	     /* epsilon_accent_color_second */
	 }
	 /* Piecharts */
	 .ewf-pie__icon,
	 .ewf-pie__percent {
	     color: #C95A2E;
	     /* epsilon_accent_color */
	 }
	 /* Map */
	 .map-info-item h5 {
	     color: #C95A2E;
	 }
	 /*	epsilon_accent_color */
	 /* ==========================================================================
	    =Navigation
	    ========================================================================== */
	 #header {
	     background-color: rgba(249, 251, 252, 1);
	     border-bottom: 1px solid #A2A2A2;
	 }
	 #header.stuck {
	     background-color: #FFFFFF;
	     border-bottom: none;
	 }
	 .sf-menu>li:hover,
	 .sf-menu>li.current {
	     -webkit-box-shadow: inset 0px 3px 0px 0px #E2E2E2 !important;
	     box-shadow: inset 0px 3px 0px 0px #E2E2E2 !important;
	     /* epsilon_menu_item_hover_color */
	 }
	 .sf-menu a,
	 #header ul a,
	 .exergie-menu li.menu-item-has-children .arrow {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 .exergie-menu-icon .exergie-navicon,
	 .exergie-menu-icon .exergie-navicon:before,
	 .exergie-menu-icon .exergie-navicon:after {
	     background-color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 .sf-menu>li:hover.arrow:before,
	 #header ul li.menu-item-has-children:hover:after {
	     /* epsilon_menu_item_hover_color */
	     color: #AC3100;
	 }
	 .sf-menu>li.arrow:before,
	 #header ul li.menu-item-has-children:after {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 .sf-menu a:hover,
	 .sf-menu a:focus,
	 #header ul a:hover,
	 #header ul a:focus,
	 #header ul li.menu-item-has-children:hover > a,
	 #header ul li.menu-item-has-children:hover > a:hover {
	     color: #AC3100;
	     /* epsilon_menu_item_hover_color */
	 }
	 .sf-menu a:active,
	 #header ul li.current-menu-item > a,
	 #header ul li.current-menu-item:after {
	     color: #2C4088;
	     /* epsilon_menu_item_active_color */
	 }
	 .sf-menu li.dropdown ul a,
	 .sf-menu li.mega .sf-mega a {}
	 .sf-menu li.dropdown li>a:hover:before {
	     border-bottom-color: #AC3100;
	     /* epsilon_menu_item_hover_color */
	 }
	 .sf-menu>li>a,
	 .sf-menu>li.dropdown>a,
	 .sf-menu>li.mega>a {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 .sf-menu>li a i {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 .sf-menu>li.current>a {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 .sf-menu>li.sfHover>a:hover,
	 .sf-menu>li>a:hover {
	     color: #AC3100;
	     /* epsilon_menu_item_hover_color */
	 }
	 .sf-menu li.dropdown ul,
	 #header ul ul {
	     background-color: #EFEFEF;
	     /* epsilon_menu_background */
	 }
	 .sf-mega {
	     background-color: #EFEFEF;
	     /* epsilon_menu_background */
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 .dropdown li:hover,
	 #header ul ul li:hover {
	     background: #E2E2E2;
	 }
	 #mobile-menu {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	     border-bottom-color: rgba(255, 255, 255, 0.25);
	     background-color: rgba(249, 251, 252, 1);
	 }
	 #mobile-menu li a {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 #mobile-menu .mobile-menu-submenu-arrow {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 #mobile-menu .mobile-menu-submenu-arrow:hover {
	     background-color: #E2E2E2;
	 }
	 #mobile-menu-trigger {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_menu_item_color */
	 }
	 #mobile-menu li>a:hover {
	     background-color: #E2E2E2;
	 }
	 #mobile-menu-trigger:hover {
	     color: #AC3100;
	     /* epsilon_menu_item_hover_color */
	 }
	 /* ==========================================================================
	    =Typography
	    ========================================================================== */
	 body,
	 .comment-metadata>a,
	 abbr[title] {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_text_color */
	 }
	 h1,
	 h2,
	 h3,
	 h4,
	 h5,
	 h6,
	 q {
	     color: 011769;
	     /* epsilon_title_color */
	 }
	 a {
	     color: #0A2175;
	 }
	 /* epsilon_link_color */
	 a:hover,
	 a:focus {
	     color: #AC3100;
	 }
	 /* epsilon_link_hover_color */
	 a:active {
	     color: #6B7598;
	 }
	 /* epsilon_link_active_color */
	 input[type="text"],
	 input[type="password"],
	 input[type="date"],
	 input[type="datetime"],
	 input[type="datetime-local"],
	 input[type="month"],
	 input[type="week"],
	 input[type="email"],
	 input[type="number"],
	 input[type="search"],
	 input[type="tel"],
	 input[type="time"],
	 input[type="url"],
	 input[type="color"],
	 textarea,
	 select {
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_text_color */
	 }
	 .ewf-btn,
	 input[type="reset"],
	 input[type="submit"],
	 input[type="button"] {
	     background-color: #C95A2E;
	 }
	 .ewf-btn:hover,
	 input[type="reset"]:hover,
	 input[type="submit"]:hover,
	 input[type="button"]:hover,
	 input[type="reset"]:focus,
	 input[type="submit"]:focus,
	 input[type="button"]:focus {
	     background-color: #2C4088;
	 }
	 input[type="reset"]:active,
	 input[type="submit"]:active,
	 input[type="button"]:active {
	     background-color: #C95A2E;
	 }
	 /* ==========================================================================
	    =Footer
	    ========================================================================== */
	 #footer {
	     background-color: #75A200;
	     /* epsilon_footer_background */
	     color: rgba(0, 0, 0, 1);
	     /* epsilon_footer_text_color */
	 }
	 #footer-bottom {
	     background-color: #6A9200;
	 }
	 #footer a {
	     color: #011769;
	 }
	 /* epsilon_footer_link_color */
	 #footer a:hover {
	     color: #EFEFEF;
	 }
	 /* epsilon_footer_link_hover_color */
	 #footer a:focus {
	     color: #EFEFEF;
	 }
	 /* epsilon_footer_link_hover_color */
	 #footer a:active {
	     color: #A2A2A2;
	 }
	 /* epsilon_footer_link_active_color */
	 #footer h1,
	 #footer h2,
	 #footer h3,
	 #footer h4,
	 #footer h5,
	 #footer h6 {
	     color: #011769;
	 }
	 #footer p {
	     color: rgba(0, 0, 0, 1);
	 }
	 /* epsilon_footer_text_color */
	 .contact-form {
	     background-color: #0377bb;
	     /* epsilon_footer_contact_background */
	 }
	 #footer .widget .widget-title,
	 #footer .widget .widget-title a {
	     color: #011769;
	     /* epsilon_footer_title_color */
	 }
	 /* ==========================================================================
	    =Blog
	    ========================================================================== */
	 #back-to-top {
	     background-color: #2C4088;
	 }
	 #back-to-top:hover {
	     background-color: #2C4088;
	 }
	 #back-to-top:hover i {
	     color: #ffffff;
	 }
	 /*
	    .sticky .post-thumbnail:after {
	    color: #2C4088;
	    }
	  */
	 .post {
	     color: rgba(0, 0, 0, 1);
	 }
	 .post-title a {
	     color: 011769;
	 }
	 .post-title a:focus,
	 .post-title a:hover {
	     color: #AC3100;
	 }
	 .post-title a:active {
	     color: #6B7598;
	 }
	 .post-meta a {
	     color: 011769;
	 }
	 .post-meta a:focus,
	 .post-meta a:hover {
	     color: #AC3100;
	 }
	 .post-meta a:active {
	     color: #6B7598;
	 }
	 .tags-links {
	     color: 011769;
	 }
	 .tags-links a {
	     border-color: #0A2175;
	     color: #0A2175;
	 }
	 .tags-links a:hover {
	     color: #AC3100;
	     border-color: #AC3100;
	 }
	 .more-link:before {
	     border-top-color: #0A2175;
	 }
	 .more-link:hover:before {
	     border-top-color: #AC3100;
	 }
	 .posted-on {
	     color: 011769;
	 }
	 .post-format {
	     color: #2C4088;
	 }
	 .pagination .page-numbers.current {
	     border-bottom-color: #2C4088;
	 }
	 .pagination .page-numbers:hover {
	     color: #2C4088;
	 }
	 .pagination .page-numbers:active {
	     color: $#2C4088;
	 }
	 .pagination .prev.page-numbers,
	 .pagination .next.page-numbers,
	 .pagination .prev.page-numbers:active,
	 .pagination .next.page-numbers:active {
	     border: 1px solid rgba(0, 0, 0, 1);
	     color: rgba(0, 0, 0, 1);
	 }
	 .pagination .prev.page-numbers:focus,
	 .pagination .prev.page-numbers:hover,
	 .pagination .next.page-numbers:focus,
	 .pagination .next.page-numbers:hover {
	     border: 1px solid #2C4088;
	     background: #2C4088;
	     color: #fff;
	 }
	 .page-links {
	     color: 011769;
	 }
	 .post-navigation {
	     border-top-color: #ebebeb;
	 }
	 .post-navigation .nav-subtitle {
	     color: 011769;
	 }
	 .author-bio {
	     border-color: #ebebeb;
	     background-color: $background-color;
	 }
	 .author-bio-social a {
	     color: 011769;
	 }
	 .no-comments,
	 .comment-awaiting-moderation {
	     color: 011769;
	 }
	 .comment-metadata>a {
	     color: rgba(0, 0, 0, 1);
	 }
	 .comment .comment-body {
	     border-top-color: #ebebeb;
	 }
	 #comment-nav-above {
	     border-bottom-color: #ebebeb;
	 }
	 #comment-nav-below {
	     border-top: 1px solid #ebebeb;
	 }
	 .widget_archive li {
	     border-bottom-color: #ebebeb;
	 }
	 .widget .widget-title,
	 .widget .widget-title a {
	     color: #C95A2E;
	 }
	 #wp-calendar caption {
	     background-color: #ffffff;
	     border-color: #ebebeb;
	     color: 011769;
	 }
	 #wp-calendar th {
	     color: 011769;
	 }
	 #wp-calendar tbody td a {
	     background-color: #2C4088;
	     color: #ffffff;
	 }
	 #wp-calendar tbody td a:hover {
	     background-color: #2C4088;
	 }
	 #wp-calendar #prev a:focus,
	 #wp-calendar #prev a:hover {
	     background-color: #2C4088;
	     border-color: #2C4088;
	 }
	 #wp-calendar #prev a:before {
	     color: #2C4088;
	 }
	 #wp-calendar #prev a:hover:before {
	     color: #fff;
	 }
	 #wp-calendar #next a:before {
	     color: #2C4088;
	 }
	 #wp-calendar #next a:hover:before {
	     color: #fff;
	 }
	 #wp-calendar #next a:focus,
	 #wp-calendar #next a:hover {
	     background-color: #2C4088;
	     border-color: #2C4088;
	 }
	 .widget_categories li {
	     border-bottom-color: #ebebeb;
	 }
	 .widget_recent_entries ul li {
	     border-bottom-color: #ebebeb;
	 }
	 .widget_rss .rss-date,
	 .widget_rss cite {
	     color: rgba(0, 0, 0, 1);
	 }
	 .widget_search .search-submit {
	     background-color: #ffffff;
	     border-color: #ebebeb;
	     color: #2C4088;
	 }
	 .widget_search .search-submit:hover,
	 .widget_search .search-submit:focus {
	     background-color: #ffffff;
	     border-color: #ebebeb;
	     color: #AC3100;
	 }
	 .widget_search .search-submit:active {
	     background-color: #ffffff;
	     border-color: #ebebeb;
	     color: 011769;
	 }
	 .widget_tag_cloud a {
	     border-color: #0A2175;
	     color: #0A2175;
	 }
	 .widget_tag_cloud a:hover {
	     color: #AC3100;
	     border-color: #AC3100;
	 }
	 .widget a {
	     color: #0A2175;
	 }
	 .widget a:hover,
	 .widget a:focus {
	     color: #AC3100;
	 }
	 .widget a:active {
	     color: #6B7598;
	 }
	</style>
	<link rel='stylesheet' id='tiko-style-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/css/tiko-style.css?ver=5.2.1' type='text/css' media='all' />
	<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'>
	</script>
	<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery-migrate.js?ver=1.4.1'>
	</script>
	<link rel='https://api.w.org/' href='http://localhost/exergieafrique.dev/index.php?rest_route=/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/exergieafrique.dev/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/exergieafrique.dev/wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 5.2.1" />
	<link rel="canonical" href="http://localhost/exergieafrique.dev/" />
	<link rel='shortlink' href='http://localhost/exergieafrique.dev/' />
	<link rel="alternate" type="application/json+oembed" href="http://localhost/exergieafrique.dev/index.php?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Flocalhost%2Fexergieafrique.dev%2F" />
	<link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1-150x150.png" sizes="32x32" />
	<link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" />
	<meta name="msapplication-TileImage" content="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" />
    </head>
    <body class="home page-template-default page page-id-258 wp-custom-logo sticky-header">
	<div id="header" class="exergie-classic header--no-shadow header--has-sticky-logo">
	    <div class="container">
		<div class="row">
		    <div class="col-xs-12">
			<div id="logo">
			    <a href="http://localhost/exergieafrique.dev/" class="custom-logo-link" rel="home">
				<img width="176" height="60" src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/rectangle_logo_exergie_x60.png" class="custom-logo" alt="Exergie Afrique" />
			    </a>
			    <a href="http://localhost/exergieafrique.dev/" class="custom-logo-link--sticky" rel="home" itemprop="url">
				<img class="custom-logo--sticky" src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/rectangle_logo_exergie_x60.png" itemprop="logo" width="176" height="60" alt="Exergie Afrique" />
			    </a>
			</div>
			<!-- end #logo -->
			<div class="exergie-menu-icon">
			    <div class="exergie-navicon">
			    </div>
			</div>
			<div id="custom_html-5" class="widget_text widget widget_custom_html">
			    <div class="textwidget custom-html-widget">
				<div class="header-social-icons">
				    <a target="_blank" href="http://www.twitter.com" title="Twitter" rel="noopener noreferrer">
					<i class="fa fa-twitter">
					</i>
				    </a>
				    <a target="_blank" href="http://www.facebook.com" title="Facebook" rel="noopener noreferrer">
					<i class="fa fa-facebook">
					</i>
				    </a>
				    <a target="_blank" href="https://www.linkedin.com/" title="Linkedin " rel="noopener noreferrer">
					<i class="fa fa-linkedin">
					</i>
				    </a>
				</div>
			    </div>
			</div>
			<ul id="menu" class="exergie-menu fixed">
			    <li id="menu-item-567" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-258 current_page_item menu-item-567">
				<a href="http://localhost/exergieafrique.dev/" aria-current="page">Accueil</a>
			    </li>
			    <li id="menu-item-955" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-955">
				<a href="http://localhost/exergieafrique.dev/?page_id=302">Actus</a>
			    </li>
			    <li id="menu-item-760" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-760">
				<a href="http://localhost/exergieafrique.dev/?page_id=64">Nos Services</a>
				<ul class="sub-menu">
				    <li id="menu-item-762" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-762">
					<a href="http://localhost/exergieafrique.dev/?page_id=44">PFEER &#8211; Formations</a>
					<ul class="sub-menu">
					    <li id="menu-item-766" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-766">
						<a href="http://localhost/exergieafrique.dev/?page_id=45">Thème n°1 : Mise en place d&#8217;un PSCOFE</a>
					    </li>
					    <li id="menu-item-765" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-765">
						<a href="http://localhost/exergieafrique.dev/?page_id=46">Thème n°2 : Mise en place d&#8217;un PGEER</a>
					    </li>
					    <li id="menu-item-764" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-764">
						<a href="http://localhost/exergieafrique.dev/?page_id=47">Thème n°3 : Mise en place d&#8217;un PGEER</a>
					    </li>
					    <li id="menu-item-763" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-763">
						<a href="http://localhost/exergieafrique.dev/?page_id=42">Thème n°4 :  comptabilité énergétique</a>
					    </li>
					</ul>
				    </li>
				    <li id="menu-item-761" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-761">
					<a href="http://localhost/exergieafrique.dev/?page_id=74">SCOFE-VSOFT: Analyse de facture</a>
				    </li>
				    <li id="menu-item-767" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-767">
					<a href="http://localhost/exergieafrique.dev/?page_id=43">PGEER &#8211; Audit énergétique</a>
				    </li>
				</ul>
			    </li>
			    <li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73">
				<a href="http://localhost/exergieafrique.dev/?page_id=35">À propos</a>
			    </li>
			    <li id="menu-item-565" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-565">
				<a href="http://localhost/exergieafrique.dev/?page_id=287">Contact</a>
			    </li>
			</ul>
		    </div>
		</div>
		<!-- end .row -->
	    </div>
	    <!-- end .container -->
	</div>
	<!-- #header -->
	<div id="wrap">
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="0">
		<div class="ewf-advanced-slider ewf-section-visible ewf-slider"
		     data-slider-speed="2000"
		     data-slider-autoplay="true"
		     data-slides-shown="2"
		     data-slides-scrolled="1"
		     data-slider-loop="true"
		     data-slider-enable-pager="false"
		     data-slider-enable-controls="false">
		    <ul class="ewf-slider__slides">
			<li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/énergies-renouvelables.jpg);height:45vh;;">
			    <div class="ewf-slider-slide__overlay" style="background-color:rgba(239, 239, 239, 0.4)" >
			    </div>
			    <div class="ewf-slider-slide__content ewf-valign--bottom ewf-text-align--center">
				<div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
				    <div class="ewf-like-h1" style="margin-bottom: 0; margin-top: 0;color: #011769;font-size: 45px;line-height: 56.25px;">De L'ÉNERGIE pour un développement plus DURABLE</div>
				</div>
				<!-- end .ewf-slider-slide__content -->
			    </div>
			</li>
			<li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_ampoule_courbe_x500.jpg);height:45vh;;">
			    <div class="ewf-slider-slide__overlay" style="background-color:rgba(239, 239, 239, 0.4)" >
			    </div>
			    <div class="ewf-slider-slide__content ewf-valign--bottom ewf-text-align--center">
				<div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
				    <div class="ewf-like-h1" style="margin-bottom: 0; margin-top: 0;color: #011769;font-size: 45px;line-height: 56.25px;">Réduisez vos coûts</div>
				    <p style="color: #011769;font-size: 20px;line-height: 35px;">avec VSOFT: logiciel d'analyse des factures d'éléctricité</p>
				</div>
				<!-- end .ewf-slider-slide__content -->
			    </div>
			</li>
			<li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_helmet-resting-on-solar-panel_x500.jpg);height:45vh;;">
			    <div class="ewf-slider-slide__overlay" style="background-color:rgba(239, 239, 239, 0.4)" >
			    </div>
			    <div class="ewf-slider-slide__content ewf-valign--bottom ewf-text-align--center">
				<div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
				    <div class="ewf-like-h1" style="margin-bottom: 0; margin-top: 0;color: #011769;font-size: 50px;line-height: 62.5px;">Audit Energétique et accompagnement de vos projets écologiques</div>
				    <p style="color: #011769;font-size: 20px;line-height: 35px;">Ensemble,</br>
					passons au <b style="color:#6A9200">vert</b> !</p>
				</div>
				<!-- end .ewf-slider-slide__content -->
			    </div>
			</li>
			<li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_formation_x500.png);height:45vh;;">
			    <div class="ewf-slider-slide__overlay" style="background-color:rgba(239, 239, 239, 0.4)" >
			    </div>
			    <div class="ewf-slider-slide__content ewf-valign--bottom ewf-text-align--center">
				<div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
				    <div class="ewf-like-h1" style="margin-bottom: 0; margin-top: 0;color: #011769;font-size: 50px;line-height: 62.5px;">Formations en gestion durable</div>
				    <p style="color: #011769;font-size: 20px;line-height: 35px;">De nouvelles capacités pour de nouvelles énergies.</p>
				</div>
				<!-- end .ewf-slider-slide__content -->
			    </div>
			</li>
		    </ul>
		    <!-- end .slides -->
		    <div class="ewf-slider__pager ewf-slider__pager--align-center">
		    </div>
		    <div class="ewf-slider__arrows">
		    </div>
		</div>
		<!-- end .advanced-slider -->
	    </section>
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="1" data-customizer-section-string-id="services">
		<style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		</style>
		<style type="text/css" media="all">#nos-services{margin-top:0px;padding-bottom:0px}</style>
		<div id="nos-services" class="section-services section ewf-section contrast ewf-section-visible ewf-valign--middle ewf-text-align--center ewf-section--title-top" >
		    <div class="ewf-section__content">
			<div class="container-fluid ewf-padding-right--none ewf-padding-left--none">
			    <div class="row ">
				<!-- Check if we have a title/subtitle -->
				<div class="col-sm-12">
				    <div class="ewf-section-text">
					<div class="headline">
					    <h3>Nos Services</h3>
					</div>
				    </div>
				    <!--/.ewf-section-text-->
				</div>
				<!--/.col-->
				<!-- // End Title Check -->
				<!-- Check if we have values in our field repeater -->
				<div class="col-sm-12 ewf-content__wrap">
				    <div class="row">
					<div class="col-sm-4 ewf-item__spacing-lg">
					    <div class="services-item ewf-item__no-effect" style="background-color: ">
						<i class="ewf-icon ion-university" style="color:#C95A2E;font-size:60px;background-color:;border-color:;border-width:5px;border-radius:100px;padding:50px;">
						</i>
						<div class="ewf-like-h6">
						    Formation en gestion durable														</div>
						<!--/.ewf-like-h6-->
					    </div>
					    <!--/.services-item-->
					</div>
					<!--/.col-sm-->
					<div class="col-sm-4 ewf-item__spacing-lg">
					    <div class="services-item ewf-item__no-effect" style="background-color: ;background-color: ">
						<i class="ewf-icon fa fa-leaf" style="color:rgba(201, 90, 46, 1);font-size:60px;background-color:;border-color:C95A2E;border-width:5px;border-radius:100px;padding:50px;">
						</i>
						<div class="ewf-like-h6">
						    Développement de projets en énergies vertes
						</div>
						<!--/.ewf-like-h6-->
					    </div>
					    <!--/.services-item-->
					</div>
					<!--/.col-sm-->
					<div class="col-sm-4 ewf-item__spacing-lg">
					    <div class="services-item ewf-item__no-effect" style="background-color: ;background-color: ;background-color: ">
						<i class="ewf-icon fa fa-dollar" style="color:#C95A2E;font-size:60px;background-color:;border-color:;border-width:5px;border-radius:100px;padding:50px;">
						</i>
						<div class="ewf-like-h6">
						    VSOFT: Analyse et réduction des coûts énergétiques														</div>
						<!--/.ewf-like-h6-->
					    </div>
					    <!--/.services-item-->
					</div>
					<!--/.col-sm-->
				    </div>
				    <!--/.col-sm--->
				</div>
				<!--/.row-->
			    </div>
			</div>
		    </div>
	    </section>
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="2" data-customizer-section-string-id="about">
		<style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		</style>
		<div id="resume-pfeer" class="section-about section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--left ewf-section--title-right" >
		    <div class="ewf-section__content">
			<div class="container">
			    <div class="row row-flow-reverse">
				<div class="col-md-6">
				    <div class="ewf-section-text">
					<div class="headline">
					    <h3>PFEER</h3>
					    <span>Programme de Formation en Efficacité Énergétique et Énergie Renouvelable</span>
					</div>
					<p>Afin de repositionner l’énergie durable comme moyen efficace de rentabilité en entreprise, <b>Exergie Afrique</b> et son partenaire <b>CEGCI</b>, proposent un plan de renforcement des capacités intitulé « <b>Accès à l’énergie durable et ses avantages en Entreprise </b>».</p>
					<p>Ce plan de formations offre aux managers et gestionnaires ainsi qu’aux équipes techniques les connaissances requises pour la gestion quotidienne de leur entreprise, mais aussi pour discuter avec les professionnels en énergie lors de l’implantation des mesures d’énergie durable.</p>
					<a class="ewf-btn ewf-btn--huge" style="background-color:rgba(201, 91, 46, 1);color:#ffffff;border-color:#011769;border-radius:5px;" href="http://localhost/exergieafrique.dev/?page_id=44">Voirs tous nos programmes !</a>
				    </div>
				    <!--/.ewf-section-text-->
				</div>
				<!--/.col-md-->
				<div class="col-md-6">
				    <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_formation_x500.png" />
				</div>
				<!--/.col-md--6-->
			    </div>
			    <!--/.row-->
			</div>
			<!--/.container class-->
		    </div>
		    <!--/.ewf-section--content-->
		</div>
		<!--/.attr-helper-->
	    </section>
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="3" data-customizer-section-string-id="about">
		<style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		</style>
		<div id="resume-pgeer" class="section-about section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--right ewf-section--title-left" >
		    <div class="ewf-section__content">
			<div class="container">
			    <div class="row ">
				<div class="col-md-6">
				    <div class="ewf-section-text">
					<div class="headline">
					    <h3>PGEER</h3>
					    <span>Programme pour l'efficacité énergétique </span>
					</div>
					<p>La mise en place d’un programme d’efficacité énergétique et d’énergie renouvelable dans les entreprises offre des avantages pour les entreprises elles-mêmes ainsi que pour l'économie dans son ensemble. Elle offre aux compagnies des possibilités d’investissement permettant d’amortir aisément leur frais et de réaliser des profits substantiels dans des délais relativement courts.</p>
					<p>Exergie Afrique intervient dans la planification, l’exécution et le suivi de vos projet</p>
					<a class="ewf-btn ewf-btn--huge" style="background-color:rgba(201, 91, 46, 1);color:#ffffff;border-color:#011769;border-radius:5px;" href="http://localhost/exergieafrique.dev/?page_id=43">En savoir plus !</a>
				    </div>
				    <!--/.ewf-section-text-->
				</div>
				<!--/.col-md-->
				<div class="col-md-6">
				    <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_helmet-resting-on-solar-panel_x500.jpg" />
				</div>
				<!--/.col-md--6-->
			    </div>
			    <!--/.row-->
			</div>
			<!--/.container class-->
		    </div>
		    <!--/.ewf-section--content-->
		</div>
		<!--/.attr-helper-->
	    </section>
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="4" data-customizer-section-string-id="about">
		<style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		</style>
		<div id="resume-pscofe" class="section-about section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--left ewf-section--title-right" >
		    <div class="ewf-section__content">
			<div class="container">
			    <div class="row row-flow-reverse">
				<div class="col-md-6">
				    <div class="ewf-section-text">
					<div class="headline">
					    <h3>VSOFT - PSCOFE</h3>
					    <span>Programme Global d’Efficacité Énergétique et d’Énergie Renouvelable</span>
					</div>
					<p>ayer le juste prix de l’électricité débarrassée des anomalies, des pénalités, des fraudes et de l’arbitraire tout en tirant profit des incitatifs tarifaires que les gouvernements offrent pour soutenir la croissance. Les économies financières annuelles générées grâce au PSCOFE varient entre 5 et 20 % des dépenses initiales avec un temps de retour sur l’investissement inférieur à un an. </p>
					<a class="ewf-btn ewf-btn--huge" style="background-color:rgba(201, 91, 46, 1);color:#ffffff;border-color:#011769;border-radius:5px;" href="http://localhost/exergieafrique.dev/?page_id=74">Faites des économies d'éléctricité !</a>
				    </div>
				    <!--/.ewf-section-text-->
				</div>
				<!--/.col-md-->
				<div class="col-md-6">
				    <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_ampoule_courbe_x500.jpg" />
				</div>
				<!--/.col-md--6-->
			    </div>
			    <!--/.row-->
			</div>
			<!--/.container class-->
		    </div>
		    <!--/.ewf-section--content-->
		</div>
		<!--/.attr-helper-->
	    </section>
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="5">
		<style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		</style>
		<div class="section-clientlist section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--center" >
		    <div class="ewf-section__content">
			<div class="container">
			    <div class="row ">
				<div class="col-sm-12">
				    <div class="ewf-section-text">
					<div class="headline">
					    <h3>Ils nous ont fait confiance</h3>
					    <span>Clients et Partenaires</span>
					</div>
				    </div>
				    <!--/.ewf-section--text-->
				</div>
				<!--/.col-->
				<div class="col-sm-12 ewf-content__wrap">
				    <div class="ewf-slider" data-slider-mode-fade="false"
					 data-slider-speed="500"
					 data-slider-autoplay="true"
					 data-slides-shown="6"
					 data-slides-scrolled="1"
					 data-slider-loop="true"
					 data-slider-enable-pager="false"
					 data-slider-enable-controls="true">
					<ul class="ewf-slider__slides">
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/boad_logo.jpg" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="Pisam">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/logo_cie.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/pisam_logo.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/cli_pat_logo-sodeci.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/Flag_of_Togo.svg_.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/Coat_of_arms_of_Gabon.svg_.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/Asecna-logo.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/cli_pat_logo-ifdd2.jpg" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/unemaf_logo.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/socfin_sogb.jpeg" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/sococe_logo-1.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					    <div class="col-sm-4">
						<li class="ewf-partner ewf-item__no-effect" style="">
						    <a href="#">
							<img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/cli_pat_logo_envipur.png" alt="">
						    </a>
						</li>
						<!-- end .ewf-partner -->
					    </div>
					    <!--/.col-sm-->
					</ul>
					<!-- end .ewf-partner-slider__slides -->
					<div class="ewf-slider__pager">
					</div>
					<div class="ewf-slider__arrows">
					</div>
				    </div>
				    <!-- end .ewf-slider -->
				</div>
				<!-- content class -->
			    </div>
			    <!--row class-->
			</div>
			<!-- container class -->
		    </div>
		    <!--/.ewf-section--content-->
		</div>
		<!--/ generate attr -->
	    </section>
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="6" data-customizer-section-string-id="cta">
		<style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		</style>
		<style type="text/css" media="all">#call-action{background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/énergies-renouvelables.jpg);background-position:center;background-size:cover;background-repeat:no-repeat}</style>
		<div id="call-action" class="section-cta section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--center ewf-section--parallax" >
		    <div class="ewf-section__overlay-color"  style="background-color:rgba(147, 189, 42, 0.5);" >
		    </div>
		    <div class="ewf-section__content">
			<div class="container">
			    <div class="row ">
				<div class="col-md-12">
				    <div class="ewf-section-text">
					<div class="headline">
					    <h3>Nous faisons rimer <b style="color:#6A9200">Écologie</b>, <b style="color:#9B2C00">Économies</b> et <b style="color:black">Profits</b> !</h3>
					</div>
				    </div>
				    <!--/.ewf-section--text-->
				</div>
				<!--/ header class-->
				<div class="col-md-12">
				    <a class="ewf-btn ewf-btn--huge" style="background-color:rgba(249, 250, 252, 0.4);color:rgba(0, 0, 0, 1);border-color:rgba(249, 250, 252, 0.4);border-radius:0px;" href="">Pour plus de Renseignements,</a>
				    <a class="ewf-btn ewf-btn--huge" style="background-color:rgba(255, 255, 255, 0.5);color:rgba(0, 0, 0, 1);border-color:rgba(255, 255, 255, 0.5);border-radius:0px;" href="">Écrivez nous !</a>
				</div>
				<!-- content class -->
			    </div>
			    <!--/.row-->
			</div>
			<!-- container class -->
		    </div>
		    <!-- ewf-section__content-->
		</div>
		<!-- attr generator-->
	    </section>
	    <section data-customizer-section-id="exergie_repeatable_section" data-section="7" data-customizer-section-string-id="shortcodes">
		<style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		 [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
		</style>
		<style type="text/css" media="all">#contact-form{background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/énergies-renouvelables.jpg);background-position:right;background-size:cover;background-repeat:no-repeat}</style>
		<div id="contact-form" class="section-shortcodes section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--center ewf-section--parallax" >
		    <div class="ewf-section__overlay-color"  style="background-color:rgba(147, 189, 42, 0.5);" >
		    </div>
		    <div class="ewf-section__content">
			<div class="container">
			    <div class="row">
				<div class="col-sm-12">
				    <div class="headline">
					<span>(Nous vous répondons dans les 24h les jours ouvrables)</span>
				    </div>
				    <div role="form" class="wpcf7" id="wpcf7-f927-o1" lang="en-US" dir="ltr">
					<div class="screen-reader-response">
					</div>
					<form action="/exergieafrique.dev/#wpcf7-f927-o1" method="post" class="wpcf7-form" novalidate="novalidate">
					    <div style="display: none;">
						<input type="hidden" name="_wpcf7" value="927" />
						<input type="hidden" name="_wpcf7_version" value="5.1.3" />
						<input type="hidden" name="_wpcf7_locale" value="en_US" />
						<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f927-o1" />
						<input type="hidden" name="_wpcf7_container_post" value="0" />
					    </div>
					    <table style="border:0px">
						<tr>
						    <td>
							<label> Nom (requis)    <span class="wpcf7-form-control-wrap your-name">
							    <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
							</span>
							</label>
						    </td>
						    <td>
							<label> Prénom (requis)    <span class="wpcf7-form-control-wrap your-name">
							    <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
							</span>
							</label>
						    </td>
						</tr>
						<tr>
						    <td>
							<label> Téléphone (requis)    <span class="wpcf7-form-control-wrap tel-544">
							    <input type="tel" name="tel-544" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" />
							</span>
							</label>
						    </td>
						    <td>
							<label> Mail (requis)    <span class="wpcf7-form-control-wrap your-email">
							    <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
							</span>
							</label>
						    </td>
						</tr>
						<tr >
						    <td colspan="2">
							<label> Sujet    <span class="wpcf7-form-control-wrap your-subject">
							    <input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
							</span>
							</label>
						    </td>
						</tr>
						<tr >
						    <td colspan="2">
							<label> Votre Message    <span class="wpcf7-form-control-wrap your-message">
							    <textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
							    </textarea>
							</span>
							</label>
						    </td>
						</tr>
					    </table>
					    <p>
						<input type="submit" value="Envoyez" class="wpcf7-form-control wpcf7-submit" />
					    </p>
					    <div class="wpcf7-response-output wpcf7-display-none">
					    </div>
					</form>
				    </div>
				</div>
			    </div>
			</div>
		    </div>
		</div>
	    </section>
	    <div id="footer">
		<div id="footer-bottom" class="row footer-sub">
		    <!-- /// FOOTER-BOTTOM  ////////////////////////////////////////////////////////////////////////////////////////////// -->
		    <div class="container">
			<div class="row">
			    <div id="footer-bottom-widget-area-1" class="col-sm-6 ol-xs-12">
				<div class="pull-rigth">
				    Theme by: <a href="https://home.tiko.ci/">Tiko Themes</a>. All rights reserved &copy 2019.			 </div>
			    </div>
			    <!-- end .col -->
			    <div id="footer-bottom-widget-area-2" class="col-sm-6 col-xs-12">
				<div id="custom_html-7" class="widget_text widget widget_custom_html">
				    <div class="textwidget custom-html-widget">
					<div class="footer-social-icons">
					    <a target="_blank" href="http://www.twitter.com" title="Twitter" rel="noopener noreferrer">
						<i class="fa fa-twitter">
						</i>
					    </a>
					    <a target="_blank" href="http://www.facebook.com" title="Facebook" rel="noopener noreferrer">
						<i class="fa fa-facebook">
						</i>
					    </a>
					    <a target="_blank" href="https://www.linkedin.com/" title="Linkedin " rel="noopener noreferrer">
						<i class="fa fa-linkedin">
						</i>
					    </a>
					</div>
				    </div>
				</div>
				<ul id="menu-menu-de-pied-de-page" class="nav">
				    <li id="menu-item-897" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-897">
					<a href="#">Protection des données</a>
				    </li>
				    <li id="menu-item-898" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-898">
					<a href="#">Carte du site</a>
				    </li>
				    <li id="menu-item-900" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-900">
					<a href="#">facebook</a>
				    </li>
				    <li id="menu-item-901" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-901">
					<a href="#">linkedin</a>
				    </li>
				</ul>
			    </div>
			    <!-- end .col -->
			</div>
			<!-- end .row -->
		    </div>
		    <!-- end .container -->
		    <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
		</div>
		<!-- end #footer-bottom -->
	    </div>
		</div>
		<a id="back-to-top" href="#">
		    <i class="fa fa-angle-up">
		    </i>
		</a>
		<link rel='stylesheet' id='slick-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/slick/slick.css?ver=5.2.1' type='text/css' media='all' />
		<script type='text/javascript'>
		 /* <![CDATA[ */
		 var wpcf7 = {"apiSettings":{"root":"http:\/\/localhost\/exergieafrique.dev\/index.php?rest_route=\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
		 /* ]]> */
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/jquery.touchSwipe.min.js?ver=2.2.1'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/jquery.easing.js?ver=1.4.1'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/imagesloaded.pkgd.min.js?ver=4.1.0'>
		</script>
		<script type='text/javascript'>
		 /* <![CDATA[ */
		 var mtphr_dnt_vars = {"is_rtl":""};
		 /* ]]> */
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/ditty-news-ticker.min.js?ver=1558354078'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/viewport/viewport.js?ver=1.0.6'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/plyr/plyr.js?ver=1.0.6'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/js/exergie.js?ver=1.0.6'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/js/tiko-func.js?ver=1.0.6'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/js/main.js?ver=1.0.6'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/wp-embed.js?ver=5.2.1'>
		</script>
		<script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/slick/slick.js?ver=1.0.6'>
		</script>
    </body>
</html>
