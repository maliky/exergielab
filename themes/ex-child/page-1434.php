<?php get_header();
?>
<?php 
$my_posts = new WP_Query(array('post_type' => 'post', 'posts_per_page' => '4', 'category_name' => 'Informations'));
?>
<?php
	if($my_posts->have_posts()) :
?>
	<?php while($my_posts->have_posts()) : $my_posts->the_post(); ?>
		<!-- ICI TES ARTICLES PAR ARTICLES. IL FAUT SEULEMENT FAIRE UN CODE, ET CELUI-CI VA SE REPÉTER PAR ARTICLE SI ARTICLE IL Y A. -->
		
	
		<div class="column-layout">
		
			<div class="img">
		<?php the_post_thumbnail('medium'); ?>
		</div>

		<div class="titre">
			
		<h2><?php the_title(); ?></h2>
		</div>
		<div class="contenu">
		<?php 
        echo wp_trim_words( get_the_content(), 80 );
		?>
		<div class="row">
			<div class="date">
			<p>Cet article est référencé dans : <?php the_category(', '); ?>&nbsp;
			
		et à été publié à cette date <?php echo get_the_date();?></p>
		 </div>
		 </div>
		<hr></hr>
	</div>

	
	</div>
	
	<?php endwhile; ?>
<?php else: ?>
	<p>Aucune article a été trouvé.</p>
<?php endif; ?>


<?php get_footer();?>