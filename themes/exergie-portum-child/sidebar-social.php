<div id="sidebar-social-1" class="sidebar">
    <?php 
    /* On test pour voir si il y a du contenu dans la sidebar.
     *   Si il y en a on l'affiche */
    if ( is_active_sidebar( 'sidebar-social-1' ) ) : 
    dynamic_sidebar( 'sidebar-social-1' ); 
    
    /* Sinon en affiche un contenu par défaut  */
    else : ?>
	<!-- default content for this side bar -->
	<a target="_blank" href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
	<a target="_blank" href="#" title="Facebook"><i class="fa fa-facebook">    </i></a>
	<a target="_blank" href="#" title="Google Plus"> <i clas="fa fa-google-plus">    </i></a>
	<a target="_blank" href="#" title="Instagram"><i class="fa fa-instagram">    </i></a>
    <?php endif;
    /* Ajouter "<?php get_sidebar( 'sb-sidebar' ); ?>" là où il faut mettre la sidebar. */
    ?>
</div>

