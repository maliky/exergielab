// rajouter un id au élément des cibles item puis les liens pointant vers les sections
const sources = document.querySelectorAll("div.services-item i.ewf-icon");
const cibles = document.querySelectorAll("div.section-about.section");
const ciblesid = "pfeer, pgeer, pscofe".split(',')
      .map(d=>d.trim())
      .map(d=>`resume-${d}`);
var n = cibles.length;  // il y a 



// ajout d'ancres sur les sources vers les cibles
// il y a autant de sources que de sections et ça doit être dans l'ordre
for (let i=0; i<n; i++){
    let icone = sources[i];
    wrap_in_href(icone, i);
    // let cible = document.querySelector("#"+ciblesid[i]);
    // let cible = cibles[i].querySelector(".headline > h3");
    // cible.setAttribute('name', ciblesid[i]);
}


function wrap_in_href(noeud, i){
    // une fonction qui enrobe un neud dans avec les balise <a>
    var parent = noeud.parentNode;
    var wrapper = document.createElement('a');
    wrapper.setAttribute('href', '#'+ciblesid[i]);

    parent.replaceChild(wrapper, noeud);
    wrapper.appendChild(noeud);
}

