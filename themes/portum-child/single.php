<?php
/**
 * The template for displaying single pages
 *
 * @package Portum
 */

get_header();

$layout = Portum_Helper::get_layout( 'portum_blog_layout' );

?>
<div id="content">

	<?php get_template_part( 'template-parts/blog/title-area', 'single' ); ?>

	<div class="container main-container">

		<div class="row">

			<?php if (have_posts()) : ?>
			<?php while (have_posts()): the_post();?> 
		<article id="article-<?php the_ID();?>" class="article single-post">
			<?php the_post_thumbnail('', array('class' => 'img-fluid','alt' =>"Responsive image"));?>
		<?php endwhile;?>
		<?php endif;?>
			
		</article>
	    </div>
	</div>
</div>	
			
			
<?php get_footer(); ?>
