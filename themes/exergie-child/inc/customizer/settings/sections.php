<?php
/**
 * Exergie Theme Customizer Panels & Sections
 *
 * @package Exergie
 * @since   1.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Register customizer panels
 */
$panels = array(
	/**
	 * General panel
	 */
	array(
		'id'   => 'exergie_panel_general',
		'args' => array(
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Homepage Options', 'exergie' ),
		),
	),

	/**
	 * Color panel
	 */
	array(
		'id'   => 'exergie_panel_colors',
		'args' => array(
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Colors', 'exergie' ),
			'priority'       => 6,
		),
	),


	/**
	 * Content panel
	 */
	array(
		'id'   => 'exergie_panel_section_content',
		'args' => array(
			'priority'       => 9999,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'type'           => 'epsilon-panel-regular',
			'title'          => esc_html__( 'Front Page Content', 'exergie' ),
			'hidden'         => true,
		),
	),
);

/**
 * Register sections
 */
$sections = array(

	/**
	 * feedback section
	 */
	array(
		'id'   => 'exergie_feedback_section',
		'args' => array(
			'type'        => 'epsilon-section-pro',
			'title'       => esc_html__( 'Suggest a feature', 'exergie' ),
			'button_text' => esc_html__( 'Send Feedback', 'exergie' ),
			'button_url'  => esc_url_raw( 'http://bit.ly/feedback-exergie' ),
			'priority'    => 0,
		),
	),


	/**
	 * General section
	 */
	array(
		'id'   => 'exergie_header_section',
		'args' => array(
			'title'    => esc_html__( 'Header', 'exergie' ),
			'priority' => 3,
		),
	),
	array(
		'id'   => 'exergie_footer_section',
		'args' => array(
			'title'    => esc_html__( 'Footer', 'exergie' ),
			'priority' => 4,
		),
	),
	array(
		'id'   => 'exergie_typography_section',
		'args' => array(
			'title'    => esc_html__( 'Typography', 'exergie' ),
			'priority' => 5,
		),
	),

	array(
		'id'   => 'exergie_misc_section',
		'args' => array(
			'title' => esc_html__( 'Integrations', 'exergie' ),
			'panel' => 'exergie_panel_general',
			'type'  => 'outer',
		),
	),
	/**
	 * Repeatable sections container
	 */
	array(
		'id'   => 'exergie_repeatable_section',
		'args' => array(
			'title'    => esc_html__( 'Epsilon Page Builder', 'exergie' ),
			'priority' => 0,
		),
	),

	/**
	 * Theme Content Sections
	 */
	array(
		'id'   => 'exergie_contact_section',
		'args' => array(
			'title'    => esc_html__( 'Contact Information', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 0,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_counters_section',
		'args' => array(
			'title'    => esc_html__( 'Counter Section', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 1,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_testimonials_section',
		'args' => array(
			'title'    => esc_html__( 'Testimonials', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 2,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_slides_section',
		'args' => array(
			'title'    => esc_html__( 'Slides', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 3,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_advanced_slides_section',
		'args' => array(
			'title'    => esc_html__( 'Slides', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 7,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_services_section',
		'args' => array(
			'title'    => esc_html__( 'Services', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 4,
			'type'     => 'epsilon-section-doubled',
		),
	),

	array(
		'id'   => 'exergie_portfolio_section',
		'args' => array(
			'title'    => esc_html__( 'Portfolio', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 5,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_team_members_section',
		'args' => array(
			'title'    => esc_html__( 'Team Members', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 7,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_pricing_section',
		'args' => array(
			'title'    => esc_html__( 'Price Boxes', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 8,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_progress_bars_section',
		'args' => array(
			'title'    => esc_html__( 'Progress Bars Section', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 9,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_piecharts_section',
		'args' => array(
			'title'    => esc_html__( 'Pie Charts Section', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 10,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_schedule_section',
		'args' => array(
			'title'    => esc_html__( 'Schedule', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 11,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_accordion_section',
		'args' => array(
			'title'    => esc_html__( 'Accordion / FAQ', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 12,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_clientlists_section',
		'args' => array(
			'title'    => esc_html__( 'Client Logos Section', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 13,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_features_section',
		'args' => array(
			'title'    => esc_html__( 'Features', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 14,
			'type'     => 'epsilon-section-doubled',
		),
	),
	array(
		'id'   => 'exergie_iconboxes_section',
		'args' => array(
			'title'    => esc_html__( 'Icons', 'exergie' ),
			'panel'    => 'exergie_panel_section_content',
			'priority' => 15,
			'type'     => 'epsilon-section-doubled',
		),
	),
);

$visible_recommended = get_option( 'exergie_recommended_actions', false );
if ( $visible_recommended ) {
	unset( $sections[0] );
}

$collection = array(
	'panel'   => $panels,
	'section' => $sections,
);

Epsilon_Customizer::add_multiple( $collection );
