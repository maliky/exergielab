<?php
/**
 * File that renders the theme Header
 *
 * @package Exergie
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
    </head>

    <body <?php body_class() ?>>
	<?php
	/**
	 * Hook: exergie_header.
	 */
	do_action( 'exergie_header' )  // voir --> inc/template-functions.php
	?>

	<div id="wrap">
