<?php
/**
 * The sidebar containing the footer widget area.
 *
 * @package Exergie
 */

/**
 * The defined sidebars
 */
$mysidebars = array(
	'footer-sidebar-1',
	'footer-sidebar-2',
	'footer-sidebar-3',
	'footer-sidebar-4',
	'footer-sidebar-5',
	'footer-sidebar-6',
);

/**
 * We create an empty array that will keep which one of them has any active sidebars
 */
$sidebars = array();
foreach ( $mysidebars as $column ) {
	if ( is_active_sidebar( $column ) ) {
		$sidebars[] = $column;
	}
};

/**
 * Handle the sizing of the footer columns based on the user selection
 */
$footer_layout = get_theme_mod( 'exergie_footer_columns', false );
if ( ! $footer_layout ) {
	$footer_layout = Exergie_Helper::get_footer_default();
}
if ( ! is_array( $footer_layout ) ) {
	$footer_layout = json_decode( $footer_layout, true );
}

$footer_width = ( get_theme_mod( 'exergie_footer_width', false ) ? 'container-fluid' : 'container' );

?>

<div id="footer">
	
	<?php get_template_part( 'template-parts/footer/copyright' ); ?>
</div>


