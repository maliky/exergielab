<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Exergie Afrique &#8211; Maîtriser l&#039;ENERGIE pour un développement plus durable</title>
      <meta name='robots' content='noindex,follow' />
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Feed" href="http://localhost/exergieafrique.dev/index.php/feed/" />
      <link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Comments Feed" href="http://localhost/exergieafrique.dev/index.php/comments/feed/" />
      <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/localhost/exergieafrique.dev\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.1"}};
         !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='wp-block-library-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/dist/block-library/style.min.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='portum-style-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/portum/style.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/portum-child/style.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='portum-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/portum-child/style.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/portum/assets/vendors/font-awesome/font-awesome.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='ion-icons-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/portum/assets/vendors/ionicons/ion.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='portum-main-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/portum/assets/css/style-portum.css?ver=1.0.0' type='text/css' media='all' />
      <style id='portum-main-inline-css' type='text/css'>
         body{
         font-family:PT Sans;
         }
         #menu li a,#footer ul.nav{
         font-family:PT Sans;
         }
         [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #25262a}
         [data-section="2"] p{ color: #8c979e}
         [data-section="5"] h1,[data-section="5"] h2,[data-section="5"] h3,[data-section="5"] h4,[data-section="5"] h5,[data-section="5"] h6,[data-section="5"] .headline span{ color: rgba(255, 255, 255, 1)}
      </style>
      <link rel='stylesheet' id='portum-style-overrides-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/portum/assets/css/overrides.css?ver=5.1.1' type='text/css' media='all' />
      <style id='portum-style-overrides-inline-css' type='text/css'>
         /**
         - epsilon_general_separator -
         #FFC000 - epsilon_accent_color - #0385d0
         #3E4346 - epsilon_accent_color_second - #a1083a
         - epsilon_text_separator -
         #3E4346 - epsilon_title_color - #1a171c
         #777777 - epsilon_text_color - #212529
         #3e4346 - epsilon_link_color - #0385d0
         #ffc000 - epsilon_link_hover_color - #a1083a
         #3e4346 - epsilon_link_active_color - #333333
         - epsilon_menu_separator -
         #ffffff - epsilon_header_background - #151c1f
         rgba(255,255,255, .3) - epsilon_header_background_sticky - rgba(255,255,255, .3)
         rgba(255,255,255,.1) - epsilon_header_background_border_bot - rgba(255,255,255,.1)
         #ffffff - epsilon_dropdown_menu_background - #a1083a
         #ffc000 - epsilon_dropdown_menu_hover_background - #940534
         #3e4346 - epsilon_menu_item_color - #ebebeb
         #ffc000 - epsilon_menu_item_hover_color - #ffffff
         #ffc000 - epsilon_menu_item_active_color - #0385d0
         - epsilon_footer_separator -
         #ffc000 - epsilon_footer_contact_background - #0377bb
         #3e4346 - epsilon_footer_background - #192229
         #000 - epsilon_footer_sub_background - #000
         #ffffff - epsilon_footer_title_color - #ffffff
         #a9afb1 - epsilon_footer_text_color - #a9afb1
         #a9afb1 - epsilon_footer_link_color - #a9afb1
         #ffffff - epsilon_footer_link_hover_color - #ffffff
         #a9afb1 - epsilon_footer_link_active_color - #a9afb1
         */
         /* ==========================================================================
         =Accent colors
         ========================================================================== */
         .text-accent-color {
         color:  #FFC000;
         /*	epsilon_accent_color */
         }
         .text-accent-color-2 {
         color: #3E4346;
         /* epsilon_accent_color_second */
         }
         /* Hero - Slider */
         .section-slider .pager-slider li {
         background: #3E4346;
         /* epsilon_accent_color_second */
         }
         .section-slider .pager-slider li.active {
         background: #FFC000;
         /*	epsilon_accent_color */
         }
         /* Portfolio */
         .portfolio-grid-item .action .zoom,
         .portfolio-grid-item .action .link {
         background: #FFC000;
         /*	epsilon_accent_color */
         }
         .portfolio-grid-item .action .zoom:hover,
         .portfolio-grid-item .action .link:hover {
         background: #3E4346;
         /* epsilon_accent_color_second */
         }
         /* Blog */
         .title-area .page-cat-links a {
         background-color: #FFC000;
         /*	epsilon_accent_color */
         }
         .blog-news-item .news-category strong {
         background: #FFC000;
         /*	epsilon_accent_color */
         }
         .blog-news-item .news-category strong:hover {
         background: #FFC000;
         /*	epsilon_accent_color */
         }
         .blog-news-item h4 a:hover,
         .blog-news-item h4 a:focus {
         color: #ffc000;
         }
         .blog-news-item h4 a:active {
         color: #3e4346;
         }
         /* Pricing */
         .section-pricing .pricing-item .plan sup,
         .section-pricing .pricing-item .plan sub,
         .section-pricing .pricing-item .plan strong,
         .section-pricing .pricing-item ul span {
         color: #FFC000;
         /*	epsilon_accent_color */
         }
         .section-pricing .pricing-item .details {
         background-color: #FFC000;
         /*	epsilon_accent_color */
         }
         .section-pricing .pricing-item:hover .plan sup,
         .section-pricing .pricing-item:hover .plan sub,
         .section-pricing .pricing-item:hover .plan strong,
         .section-pricing .pricing-item:hover ul span {
         color: #3E4346;
         /* epsilon_accent_color_second */
         }
         .section-pricing .pricing-item:hover .details {
         background-color: #3E4346;
         /* epsilon_accent_color_second */
         }
         @media (max-width: 768px) {
         .section-pricing .pricing-item .plan sup,
         .section-pricing .pricing-item .plan sub,
         .section-pricing .pricing-item .plan strong,
         .section-pricing .pricing-item:hover .plan sup,
         .section-pricing .pricing-item:hover .plan sub,
         .section-pricing .pricing-item:hover .plan strong {
         color: #fff;
         }
         }
         /* Counters */
         .ewf-counter__odometer,
         .ewf-counter__symbol {
         color: #3E4346;
         /* epsilon_accent_color_second */
         }
         /* Team members */
         .team-members-item:hover .overlay {
         background: #FFC000;
         /*	epsilon_accent_color */
         opacity: .65;
         }
         /* Progress */
         /*
         .ewf-progress__bar-liniar-wrap {
         background-color: #3E4346;
         }
         */
         .ewf-progress__bar-liniar {
         background-color: #3E4346;
         /* epsilon_accent_color_second */
         }
         .ewf-progress--alternative-modern .ewf-progress__bar-liniar-wrap:after {
         background: #3E4346;
         /* epsilon_accent_color_second */
         }
         /* Piecharts */
         .ewf-pie__icon,
         .ewf-pie__percent {
         color: #FFC000;
         /*	epsilon_accent_color */
         }
         /* Map */
         .map-info-item h5 {
         color: #FFC000;
         }
         /*	epsilon_accent_color */
         /* ==========================================================================
         =Navigation
         ========================================================================== */

         #icon-social{
         	display: inline-block;
         	
         	position: relative;
         	margin-left: 1030px;
         }
         .aps-each-icon.icon-3-1{
         	display: inline-block;
         	
         }
         .aps-each-icon.icon-3-2{
         	display: inline-block;
         	
         }
         .aps-each-icon.icon-3-3{
         	display: inline-block;
         	
         }
         
         #header {
         background-color: #ffffff;
         border-bottom: 1px solid rgba(255,255,255,.1);
         }
         #header.stuck {
         background-color: #FFFFFF;
         border-bottom: none;
         }
         .sf-menu>li:hover,
         .sf-menu>li.current {
         -webkit-box-shadow: inset 0px 3px 0px 0px #ffc000 !important;
         box-shadow: inset 0px 3px 0px 0px #ffc000 !important;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu a,
         #header ul a,
         .portum-menu li.menu-item-has-children .arrow {
         color: #3e4346;
		font-size: 20px;
         /* epsilon_menu_item_color */
         }
         .portum-menu-icon .portum-navicon,
         .portum-menu-icon .portum-navicon:before,
         .portum-menu-icon .portum-navicon:after {
         background-color: #3e4346;
         /* epsilon_menu_item_color */
         }
         .sf-menu>li:hover.arrow:before,
         #header ul li.menu-item-has-children:hover:after {
         /* epsilon_menu_item_hover_color */
         color: #ffc000;
         }
         .sf-menu>li.arrow:before,
         #header ul li.menu-item-has-children:after {
         color: #3e4346;
         /* epsilon_menu_item_color */
         }
         .sf-menu a:hover,
         .sf-menu a:focus,
         #header ul a:hover,
         #header ul a:focus,
         #header ul li.menu-item-has-children:hover > a,
         #header ul li.menu-item-has-children:hover > a:hover {
         color: #cc662e;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu a:active,
         #header ul li.current-menu-item > a,
         #header ul li.current-menu-item:after {
         color: #cc662e;
         /* epsilon_menu_item_active_color */
         }
         .sf-menu li.dropdown ul a,
         .sf-menu li.mega .sf-mega a {}
         .sf-menu li.dropdown li>a:hover:before {
         border-bottom-color: #cc662e;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu>li>a,
         .sf-menu>li.dropdown>a,
         .sf-menu>li.mega>a {
         color: #cc662e;
         /* epsilon_menu_item_color */
         }
         .sf-menu>li a i {
         color: #cc662e;
         /* epsilon_menu_item_color */
         }
         .sf-menu>li.current>a {
         color: #cc662e;
         /* epsilon_menu_item_color */
         }
         .sf-menu>li.sfHover>a:hover,
         .sf-menu>li>a:hover {
         color: #ffc000;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu li.dropdown ul,
         #header ul ul {
         background-color: #ffffff;
         /* epsilon_menu_background */
         }
         .sf-mega {
         background-color: #ffffff;
         /* epsilon_menu_background */
         color: #3e4346;
         /* epsilon_menu_item_color */
         }
         .dropdown li:hover,
         #header ul ul li:hover {
         background: #d9d5d5;
         }
         #mobile-menu {
         color: #3e4346;
         /* epsilon_menu_item_color */
         border-bottom-color: rgba(255, 255, 255, 0.25);
         background-color: #ffffff;
         }
         #mobile-menu li a {
         color: #3e4346;
         /* epsilon_menu_item_color */
         }
         #mobile-menu .mobile-menu-submenu-arrow {
         color: #3e4346;
         /* epsilon_menu_item_color */
         }
         #mobile-menu .mobile-menu-submenu-arrow:hover {
         background-color: #ffc000;
         }
         #mobile-menu-trigger {
         color: #3e4346;
         /* epsilon_menu_item_color */
         }
         #mobile-menu li>a:hover {
         background-color: #ffc000;
         }
         #mobile-menu-trigger:hover {
         color: #ffc000;
         /* epsilon_menu_item_hover_color */
         }
         /* ==========================================================================
         =Typography
         ========================================================================== */
         body,
         .comment-metadata>a,
         abbr[title] {
         color: #777777;
         /* epsilon_text_color */
         }
         h1,
         h2,
         h3,
         h4,
         h5,
         h6,
         q {
         color: #3E4346;
         /* epsilon_title_color */
         }
         a {
         color: #3e4346;
         }
         /* epsilon_link_color */
         a:hover,
         a:focus {
         color: #cc662e;
         }
         /* epsilon_link_hover_color */
         a:active {
         color: #3e4346;
         }
         /* epsilon_link_active_color */
         input[type="text"],
         input[type="password"],
         input[type="date"],
         input[type="datetime"],
         input[type="datetime-local"],
         input[type="month"],
         input[type="week"],
         input[type="email"],
         input[type="number"],
         input[type="search"],
         input[type="tel"],
         input[type="time"],
         input[type="url"],
         input[type="color"],
         textarea,
         select {
         color: #777777;
         /* epsilon_text_color */
         }
         .ewf-btn,
         input[type="reset"],
         input[type="submit"],
         input[type="button"] {
         background-color: #FFC000;
         }
         .ewf-btn:hover,
         input[type="reset"]:hover,
         input[type="submit"]:hover,
         input[type="button"]:hover,
         input[type="reset"]:focus,
         input[type="submit"]:focus,
         input[type="button"]:focus {
         background-color: #3E4346;
         }
         input[type="reset"]:active,
         input[type="submit"]:active,
         input[type="button"]:active {
         background-color: #FFC000;
         }
         /* ==========================================================================
         =Footer
         ========================================================================== */
         #footer {
         background-color: #3e4346;
         /* epsilon_footer_background */
         color: #a9afb1;
         /* epsilon_footer_text_color */
         }
         #footer-bottom {
         background-color: #000;
         }
         #footer a {
         color: #a9afb1;
         }
         /* epsilon_footer_link_color */
         #footer a:hover {
         color: #ffffff;
         }
         /* epsilon_footer_link_hover_color */
         #footer a:focus {
         color: #ffffff;
         }
         /* epsilon_footer_link_hover_color */
         #footer a:active {
         color: #a9afb1;
         }
         /* epsilon_footer_link_active_color */
         #footer h1,
         #footer h2,
         #footer h3,
         #footer h4,
         #footer h5,
         #footer h6 {
         color: #ffffff;
         }
         #footer p {
         color: #a9afb1;
         }
         /* epsilon_footer_text_color */
         .contact-form {
         background-color: #ffc000;
         /* epsilon_footer_contact_background */
         }
         #footer .widget .widget-title,
         #footer .widget .widget-title a {
         color: #ffffff;
         /* epsilon_footer_title_color */
         }
         /* ==========================================================================
         =Blog
         ========================================================================== */
         #back-to-top {
         background-color: #3E4346;
         }
         #back-to-top:hover {
         background-color: #3E4346;
         }
         #back-to-top:hover i {
         color: #ffffff;
         }
         /*
         .sticky .post-thumbnail:after {
         color: #3E4346;
         }
         */
         .post {
         color: #777777;
         }
         .post-title a {
         color: #3E4346;
         }
         .post-title a:focus,
         .post-title a:hover {
         color: #ffc000;
         }
         .post-title a:active {
         color: #3e4346;
         }
         .post-meta a {
         color: #3E4346;
         }
         .post-meta a:focus,
         .post-meta a:hover {
         color: #ffc000;
         }
         .post-meta a:active {
         color: #3e4346;
         }
         .tags-links {
         color: #3E4346;
         }
         .tags-links a {
         border-color: #3e4346;
         color: #3e4346;
         }
         .tags-links a:hover {
         color: #ffc000;
         border-color: #ffc000;
         }
         .more-link:before {
         border-top-color: #3e4346;
         }
         .more-link:hover:before {
         border-top-color: #ffc000;
         }
         .posted-on {
         color: #3E4346;
         }
         .post-format {
         color: #3E4346;
         }
         .pagination .page-numbers.current {
         border-bottom-color: #3E4346;
         }
         .pagination .page-numbers:hover {
         color: #3E4346;
         }
         .pagination .page-numbers:active {
         color: $#3E4346;
         }
         .pagination .prev.page-numbers,
         .pagination .next.page-numbers,
         .pagination .prev.page-numbers:active,
         .pagination .next.page-numbers:active {
         border: 1px solid #777777;
         color: #777777;
         }
         .pagination .prev.page-numbers:focus,
         .pagination .prev.page-numbers:hover,
         .pagination .next.page-numbers:focus,
         .pagination .next.page-numbers:hover {
         border: 1px solid #3E4346;
         background: #3E4346;
         color: #fff;
         }
         .page-links {
         color: #3E4346;
         }
         .post-navigation {
         border-top-color: #ebebeb;
         }
         .post-navigation .nav-subtitle {
         color: #3E4346;
         }
         .author-bio {
         border-color: #ebebeb;
         background-color: $background-color;
         }
         .author-bio-social a {
         color: #3E4346;
         }
         .no-comments,
         .comment-awaiting-moderation {
         color: #3E4346;
         }
         .comment-metadata>a {
         color: #777777;
         }
         .comment .comment-body {
         border-top-color: #ebebeb;
         }
         #comment-nav-above {
         border-bottom-color: #ebebeb;
         }
         #comment-nav-below {
         border-top: 1px solid #ebebeb;
         }
         .widget_archive li {
         border-bottom-color: #ebebeb;
         }
         .widget .widget-title,
         .widget .widget-title a {
         color: #FFC000;
         }
         #wp-calendar caption {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: #3E4346;
         }
         #wp-calendar th {
         color: #3E4346;
         }
         #wp-calendar tbody td a {
         background-color: #3E4346;
         color: #ffffff;
         }
         #wp-calendar tbody td a:hover {
         background-color: #3E4346;
         }
         #wp-calendar #prev a:focus,
         #wp-calendar #prev a:hover {
         background-color: #3E4346;
         border-color: #3E4346;
         }
         #wp-calendar #prev a:before {
         color: #3E4346;
         }
         #wp-calendar #prev a:hover:before {
         color: #fff;
         }
         #wp-calendar #next a:before {
         color: #3E4346;
         }
         #wp-calendar #next a:hover:before {
         color: #fff;
         }
         #wp-calendar #next a:focus,
         #wp-calendar #next a:hover {
         background-color: #3E4346;
         border-color: #3E4346;
         }
         .widget_categories li {
         border-bottom-color: #ebebeb;
         }
         .widget_recent_entries ul li {
         border-bottom-color: #ebebeb;
         }
         .widget_rss .rss-date,
         .widget_rss cite {
         color: #777777;
         }
         .widget_search .search-submit {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: #3E4346;
         }
         .widget_search .search-submit:hover,
         .widget_search .search-submit:focus {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: #ffc000;
         }
         .widget_search .search-submit:active {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: #3E4346;
         }
         .widget_tag_cloud a {
         border-color: #3e4346;
         color: #3e4346;
         }
         .widget_tag_cloud a:hover {
         color: #ffc000;
         border-color: #ffc000;
         }
         .widget a {
         color: #3e4346;
         }
         .widget a:hover,
         .widget a:focus {
         color: #ffc000;
         }
         .widget a:active {
         color: #3e4346;
         }
      </style>
      <link rel='stylesheet' id='epsilon-google-fonts-0-css'  href='//fonts.googleapis.com/css?family=PT+Sans%3A400%2C700&#038;subset=latin-ext&#038;ver=5.1.1' type='text/css' media='all' />
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
      <link rel='https://api.w.org/' href='http://localhost/exergieafrique.dev/index.php/wp-json/' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/exergieafrique.dev/xmlrpc.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/exergieafrique.dev/wp-includes/wlwmanifest.xml" />
      <meta name="generator" content="WordPress 5.1.1" />
      <link rel="canonical" href="http://localhost/exergieafrique.dev/" />
      <link rel='shortlink' href='http://localhost/exergieafrique.dev/' />
      <link rel="alternate" type="application/json+oembed" href="http://localhost/exergieafrique.dev/index.php/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost/exergieafrique.dev%2F" />
      <link rel="alternate" type="text/xml+oembed" href="http://localhost/exergieafrique.dev/index.php/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost/exergieafrique.dev%2F&#038;format=xml" />
      <link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1-32x32.png" sizes="32x32" />
      <link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1-192x192.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1-180x180.png" />
      <meta name="msapplication-TileImage" content="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1-270x270.png" />
      <style type="text/css" id="wp-custom-css">
         #logo img {
         max-width: 150px !important;

         }
         .ewf-btn,
         input[type="submit"]{
         background-color: #03c4eb;
         border-radius: 0;
         }
         .ewf-btn.ewf-btn--huge {
         border-radius: 0;
         }
         .ewf-portfolio-item {
         margin-bottom: 0;
         margin-top: 0;
         margin-right: 1px;
         }
         #projects .ewf-section-text {
         max-width: 100% !important;
         }
         .ewf-slider .ewf-slider__slides li {
         background-attachment: fixed;
         }
         #services-overlapped .ewf-content__wrap {
         border: 1px solid #EEE;
         margin-bottom: 50px;
         }
         #services-overlapped .services-item {
         margin-bottom: 0;
         border-right: 1px solid #EEE;
         }
         .section-contact .address-contact .address-content {
         display: block;
         padding-left: 45px;
         font-style: italic;
         line-height: 2;
         }
         .section-contact .address-contact span {
         float: left;
         }
         .footer-social {
         text-align: center;
         }
         .footer-social h5 {
         font-size: 18px;
         font-weight: 900;
         }
         .footer-social-icons {
         margin-top: 15px;
         }
         .footer-social-icons a{
         margin-right: 15px;
         }
         .footer-social-icons a i{
         font-size: 24px;
         color: #CCC;
         }
      </style>
   </head>
   <body class="home page-template-default page page-id-258 page-parent wp-custom-logo sticky-header">
      <div id="header" class="portum-classic header--over-content header--no-shadow header--has-sticky-logo">
         <div class="container">
            <div class="row">
            	<div id="icon-social" class="pull-right">
<?php echo do_shortcode('[aps-social id="3"]')?>
                                           </div>
                                                 <div class="col-xs-12">
                 
                                                           <div id="logo">
                                                                  <a href="http://localhost/exergieafrique.dev/" class="custom-logo-link" rel="home" itemprop="url"><img width="1000" height="410" src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_rectangle-2-1.png" class="custom-logo" alt="Exergie Afrique" itemprop="logo" srcset="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_rectangle-2-1.png 1000w, http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_rectangle-2-1-300x123.png 300w, http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_rectangle-2-1-768x315.png 768w" sizes="(max-width: 1000px) 100vw, 1000px" /></a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <a href="http://localhost/exergieafrique.dev/" class="custom-logo-link--sticky" rel="home" itemprop="url"><img class="custom-logo--sticky" src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_rectangle-2-3.png" itemprop="logo" width="1000" height="373" alt="Exergie Afrique" /></a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  <!-- end #logo -->
                  <div class="portum-menu-icon">
                     <div class="portum-navicon"></div>
                  </div>
                  <!--fghjklkjhgfg-->
                  <?php
                 wp_nav_menu( array('menu_id' => "3", // (string) The ID that is applied to the ul element which forms the menu. Default is the menu slug, incremented
                  ) );
                 ?>
               </div>
            </div>
            <!-- end .row -->
         </div>
         <!-- end .container -->
      </div>
      <!-- #header -->
