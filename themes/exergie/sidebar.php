<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Exergie
 */

if ( is_active_sidebar( 'sidebar' ) ) {
	dynamic_sidebar( 'sidebar' );
}
