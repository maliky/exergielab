<?php
/**
 * The template for displaying pages
 *
 * @package Exergie
 */

get_header();

$exergie_fp = Epsilon_Page_Generator::get_instance( 'exergie_frontpage_sections_' . get_the_ID(), get_the_ID() );

// récupère l'image mise en vedette de cette page
$img = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'thumbnail' ); 

$layout = Exergie_Helper::get_layout();

if ( ! empty( $exergie_fp->sections ) ) :
	  $exergie_fp->generate_output();
else :
?>
    <div id="content" "page-content">
	<div class="container page-container">
	    <div class="row">
		<!-- <div class="col-md-1"></div>
		     <div class="col-md-10">
		     <div class="intro-item">
		     <h4><?php the_title(); ?></h4>
		     </div>
		     </div>
		     <div class="col-md-1"></div> -->
	    </div>
	    <div class="row">
		<?php
		/* Ajouter le test pour les autres sidebar */
		if ( is_active_sidebar( 'sidebar' ) ) {
		?>
		    <div class="col-sm-<?php echo esc_attr( $layout['columns']['sidebar']['span'] ); ?> page-sidebar-col">
			<!-- /// SIDEBAR CONTENT  /////////////////////////////////////////////////////////////////////////////////// -->
			<div id="img-page-<?php get_the_ID() ?>" class="page-image">
			    <img src="<?php echo esc_url( $img ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"/>
			</div> 

			<?php dynamic_sidebar( 'sidebar' ); ?>

			<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////// -->
		    </div>
		<?php
		}
		?>

		<div class="<?php echo ( 1 === $layout['columnsCount'] && ! is_active_sidebar( 'sidebar' ) ) ? 'col-sm-12' : 'col-sm-' . esc_attr( $layout['columns']['content']['span'] ); ?>">
		    <!-- /// MAIN CONTENT  ////////////////////////////////////////////////////////////////////////////////////// -->
		    <?php
		    if ( have_posts() ) :
		    while ( have_posts() ) :
		    the_post();
		    get_template_part( 'template-parts/content/content', 'page' );
		    endwhile;
		    else :
		    get_template_part( 'template-parts/content/content', 'none' );
		    endif;

		    ?>
		    <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////// -->
		</div>

		<?php
		if ( 'right-sidebar' === $layout['type'] && is_active_sidebar( 'sidebar' ) ) {
		?>
		    <div class="col-sm-<?php echo esc_attr( $layout['columns']['sidebar']['span'] ); ?>">
			<!-- /// SIDEBAR CONTENT  /////////////////////////////////////////////////////////////////////////////////// -->
			<?php dynamic_sidebar( 'sidebar' ); ?>
			<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////// -->
		    </div>
		<?php
		}
		?>
	    </div>
	</div>
    </div>
<?php
endif;

get_footer();
