(function( $ ) {
	'use strict';
	/**
	 * Window scroll events
	 */
	$( window ).scroll( function() {
		/**
		 * Initiate plugins
		 */
		try {
			Exergie.Plugins.animateCounters();
			Exergie.Plugins.animateProgress();
			Exergie.Plugins.animatePieCharts();
			Exergie.Theme.hideBackTop();
			Exergie.Theme.header();
		} catch ( error ) {

		}
	} );

	/**
	 * Window resize event
	 */
	$( window ).resize( function() {
		Exergie.Plugins.animatePieCharts();
		Exergie.Plugins.setDimensionsPieCharts();
		Exergie.Theme.header();
	} );

	/**
	 * Document ready event
	 */
	$( document ).ready( function( $ ) {
		/**
		 * Initiate plugins
		 */
		try {
			Exergie.Plugins.advancedSlider();
			Exergie.Plugins.video();
			Exergie.Plugins.magnificPopup();
			Exergie.Plugins.animateCounters();
			Exergie.Plugins.animateProgress();
			Exergie.Plugins.animatePieCharts();
			Exergie.Plugins.setDimensionsPieCharts();

			/**
			 * Initiate Theme related functions
			 */
			Exergie.Theme.header();
			Exergie.Theme.menu();
			Exergie.Theme.classicMenu();
			Exergie.Theme.offCanvasMenu();
			Exergie.Theme.map();
			Exergie.Theme.backTop();
			Exergie.Theme.hideBackTop();
			Exergie.Theme.smoothScroll();
			Exergie.Theme.handleAccordions();

		} catch ( error ) {
			console.log( error );
		}
	} );

	$( document ).on( 'epsilon-selective-refresh-ready', function() {
		/**
		 * Initiate plugins
		 */
		Exergie.Plugins.advancedSlider();
		Exergie.Plugins.video();
		Exergie.Plugins.magnificPopup();
		Exergie.Plugins.animateCounters();
		Exergie.Plugins.animateProgress();
		Exergie.Plugins.animatePieCharts();
		Exergie.Plugins.setDimensionsPieCharts();

		/**
		 * Initiate theme scripts
		 */
		Exergie.Theme.map();
		Exergie.Theme.header();
		Exergie.Theme.menu();
		Exergie.Theme.classicMenu();
		Exergie.Theme.offCanvasMenu();

		/**
		 * Initiate Event related functions
		 */

	} );

})( jQuery );