

<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Qui sommes nous ? &#8211; Exergie Afrique</title>
      <meta name='robots' content='noindex,follow' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Feed" href="http://localhost/exergieafrique.dev/?feed=rss2" />
      <link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Comments Feed" href="http://localhost/exergieafrique.dev/?feed=comments-rss2" />
      <link rel="alternate" type="text/calendar" title="Exergie Afrique &raquo; iCal Feed" href="http://localhost/exergieafrique.dev?post_type=tribe_events&#038;ical=1" />
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/localhost\/exergieafrique.dev\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.1"}};
         !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='dashicons-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/dashicons.min.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='admin-bar-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/admin-bar.min.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tribe-tooltip-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/the-events-calendar/common/src/resources/css/tooltip.min.css?ver=4.9.9' type='text/css' media='all' />
      <link rel='stylesheet' id='tribe-events-admin-menu-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/the-events-calendar/src/resources/css/admin-menu.min.css?ver=4.9.2' type='text/css' media='all' />
      <link rel='stylesheet' id='elusive-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/vendor/codeinwp/icon-picker/css/types/elusive.min.css?ver=2.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/vendor/codeinwp/icon-picker/css/types/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
      <link rel='stylesheet' id='foundation-icons-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/vendor/codeinwp/icon-picker/css/types/foundation-icons.min.css?ver=3.0' type='text/css' media='all' />
      <link rel='stylesheet' id='menu-icons-extra-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/css/extra.min.css?ver=0.11.5' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-block-library-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/dist/block-library/style.min.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3' type='text/css' media='all' />
      <link rel='stylesheet' id='ditty-news-ticker-font-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/libs/fontastic/styles.css?ver=2.2.1' type='text/css' media='all' />
      <link rel='stylesheet' id='ditty-news-ticker-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/css/style.css?ver=1559147953' type='text/css' media='all' />
      <link rel='stylesheet' id='widgetopts-styles-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/widget-options/assets/css/widget-options.css' type='text/css' media='all' />
      <link rel='stylesheet' id='exergie-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/style.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='ion-icons-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/ionicons/ion.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='exergie-main-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/css/style-exergie.css?ver=1.0.6' type='text/css' media='all' />
      <style id='exergie-main-inline-css' type='text/css'>
      </style>
      <link rel='stylesheet' id='exergie-style-overrides-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/css/overrides.css?ver=5.1.1' type='text/css' media='all' />
      <style id='exergie-style-overrides-inline-css' type='text/css'>
         /**
         - epsilon_general_separator -
         #C95A2E - epsilon_accent_color - #0385d0
         #2C4088 - epsilon_accent_color_second - #a1083a
         - epsilon_text_separator -
         011769 - epsilon_title_color - #1a171c
         rgba(0, 0, 0, 1) - epsilon_text_color - #212529
         #0A2175 - epsilon_link_color - #0385d0
         #AC3100 - epsilon_link_hover_color - #a1083a
         #6B7598 - epsilon_link_active_color - #333333
         - epsilon_menu_separator -
         rgba(249, 251, 252, 1) - epsilon_header_background - #151c1f
         #FFFFFF - epsilon_header_background_sticky - rgba(255,255,255,.3)
         #A2A2A2 - epsilon_header_background_border_bot - rgba(255,255,255,.1)
         #EFEFEF - epsilon_dropdown_menu_background - #a1083a
         #E2E2E2 - epsilon_dropdown_menu_hover_background - #940534
         rgba(0, 0, 0, 1) - epsilon_menu_item_color - #ebebeb
         #AC3100 - epsilon_menu_item_hover_color - #ffffff
         #2C4088 - epsilon_menu_item_active_color - #0385d0
         - epsilon_footer_separator -
         #0377bb - epsilon_footer_contact_background - #0377bb
         #75A200 - epsilon_footer_background - #192229
         #6A9200 - epsilon_footer_sub_background - #000
         #011769 - epsilon_footer_title_color - #ffffff
         rgba(0, 0, 0, 1) - epsilon_footer_text_color - #a9afb1
         #011769 - epsilon_footer_link_color - #a9afb1
         #EFEFEF - epsilon_footer_link_hover_color - #ffffff
         #A2A2A2 - epsilon_footer_link_active_color - #a9afb1
         */
         /* ==========================================================================
         =Accent colors
         ========================================================================== */
         .text-accent-color {
         color:  #C95A2E;
         /*	epsilon_accent_color */
         }
         .text-accent-color-2 {
         color: #2C4088;
         /* epsilon_accent_color_second */
         }
         /* Hero - Slider */
         .section-slider .pager-slider li {
         background: #2C4088;
         /* epsilon_accent_color_second */
         }
         .section-slider .pager-slider li.active {
         background: #C95A2E;
         /*	epsilon_accent_color */
         }
         /* Portfolio */
         .portfolio-grid-item .action .zoom,
         .portfolio-grid-item .action .link {
         background: #C95A2E;
         /*	epsilon_accent_color */
         }
         .portfolio-grid-item .action .zoom:hover,
         .portfolio-grid-item .action .link:hover {
         background: #2C4088;
         /* epsilon_accent_color_second */
         }
         /* Blog */
         .title-area .page-cat-links a {
         background-color: #C95A2E;
         /*	epsilon_accent_color */
         }
         .blog-news-item .news-category strong {
         background: #C95A2E;
         /*	epsilon_accent_color */
         }
         .blog-news-item .news-category strong:hover {
         background: #C95A2E;
         /*	epsilon_accent_color */
         }
         .blog-news-item h4 a:hover,
         .blog-news-item h4 a:focus {
         color: #AC3100;
         }
         .blog-news-item h4 a:active {
         color: #6B7598;
         }
         /* Pricing */
         .section-pricing .pricing-item .plan sup,
         .section-pricing .pricing-item .plan sub,
         .section-pricing .pricing-item .plan strong,
         .section-pricing .pricing-item ul span {
         color: #C95A2E;
         /*	epsilon_accent_color */
         }
         .section-pricing .pricing-item .details {
         background-color: #C95A2E;
         /*	epsilon_accent_color */
         }
         .section-pricing .pricing-item:hover .plan sup,
         .section-pricing .pricing-item:hover .plan sub,
         .section-pricing .pricing-item:hover .plan strong,
         .section-pricing .pricing-item:hover ul span {
         color: #2C4088;
         /* epsilon_accent_color_second */
         }
         .section-pricing .pricing-item:hover .details {
         background-color: #2C4088;
         /* epsilon_accent_color_second */
         }
         @media (max-width: 768px) {
         .section-pricing .pricing-item .plan sup,
         .section-pricing .pricing-item .plan sub,
         .section-pricing .pricing-item .plan strong,
         .section-pricing .pricing-item:hover .plan sup,
         .section-pricing .pricing-item:hover .plan sub,
         .section-pricing .pricing-item:hover .plan strong {
         color: #fff;
         }
         }
         /* Counters */
         .ewf-counter__odometer,
         .ewf-counter__symbol {
         color: #2C4088;
         /* epsilon_accent_color_second */
         }
         /* Team members */
         .team-members-item:hover .overlay {
         background: #C95A2E;
         /*	epsilon_accent_color */
         opacity: .65;
         }
         /* Progress */
         /*
         .ewf-progress__bar-liniar-wrap {
         background-color: #2C4088;
         }
         */
         .ewf-progress__bar-liniar {
         background-color: #2C4088;
         /* epsilon_accent_color_second */
         }
         .ewf-progress--alternative-modern .ewf-progress__bar-liniar-wrap:after {
         background: #2C4088;
         /* epsilon_accent_color_second */
         }
         /* Piecharts */
         .ewf-pie__icon,
         .ewf-pie__percent {
         color: #C95A2E;
         /*	epsilon_accent_color */
         }
         /* Map */
         .map-info-item h5 {
         color: #C95A2E;
         }
         /*	epsilon_accent_color */
         /* ==========================================================================
         =Navigation
         ========================================================================== */
         #header {
         background-color: rgba(249, 251, 252, 1);
         border-bottom: 1px solid #A2A2A2;
         }
         #header.stuck {
         background-color: #FFFFFF;
         border-bottom: none;
         }
         .sf-menu>li:hover,
         .sf-menu>li.current {
         -webkit-box-shadow: inset 0px 3px 0px 0px #E2E2E2 !important;
         box-shadow: inset 0px 3px 0px 0px #E2E2E2 !important;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu a,
         #header ul a,
         .exergie-menu li.menu-item-has-children .arrow {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         .exergie-menu-icon .exergie-navicon,
         .exergie-menu-icon .exergie-navicon:before,
         .exergie-menu-icon .exergie-navicon:after {
         background-color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         .sf-menu>li:hover.arrow:before,
         #header ul li.menu-item-has-children:hover:after {
         /* epsilon_menu_item_hover_color */
         color: #AC3100;
         }
         .sf-menu>li.arrow:before,
         #header ul li.menu-item-has-children:after {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         .sf-menu a:hover,
         .sf-menu a:focus,
         #header ul a:hover,
         #header ul a:focus,
         #header ul li.menu-item-has-children:hover > a,
         #header ul li.menu-item-has-children:hover > a:hover {
         color: #AC3100;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu a:active,
         #header ul li.current-menu-item > a,
         #header ul li.current-menu-item:after {
         color: #2C4088;
         /* epsilon_menu_item_active_color */
         }
         .sf-menu li.dropdown ul a,
         .sf-menu li.mega .sf-mega a {}
         .sf-menu li.dropdown li>a:hover:before {
         border-bottom-color: #AC3100;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu>li>a,
         .sf-menu>li.dropdown>a,
         .sf-menu>li.mega>a {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         .sf-menu>li a i {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         .sf-menu>li.current>a {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         .sf-menu>li.sfHover>a:hover,
         .sf-menu>li>a:hover {
         color: #AC3100;
         /* epsilon_menu_item_hover_color */
         }
         .sf-menu li.dropdown ul,
         #header ul ul {
         background-color: #EFEFEF;
         /* epsilon_menu_background */
         }
         .sf-mega {
         background-color: #EFEFEF;
         /* epsilon_menu_background */
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         .dropdown li:hover,
         #header ul ul li:hover {
         background: #E2E2E2;
         }
         #mobile-menu {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         border-bottom-color: rgba(255, 255, 255, 0.25);
         background-color: rgba(249, 251, 252, 1);
         }
         #mobile-menu li a {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         #mobile-menu .mobile-menu-submenu-arrow {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         #mobile-menu .mobile-menu-submenu-arrow:hover {
         background-color: #E2E2E2;
         }
         #mobile-menu-trigger {
         color: rgba(0, 0, 0, 1);
         /* epsilon_menu_item_color */
         }
         #mobile-menu li>a:hover {
         background-color: #E2E2E2;
         }
         #mobile-menu-trigger:hover {
         color: #AC3100;
         /* epsilon_menu_item_hover_color */
         }
         /* ==========================================================================
         =Typography
         ========================================================================== */
         body,
         .comment-metadata>a,
         abbr[title] {
         color: rgba(0, 0, 0, 1);
         /* epsilon_text_color */
         }
         h1,
         h2,
         h3,
         h4,
         h5,
         h6,
         q {
         color: 011769;
         /* epsilon_title_color */
         }
         a {
         color: #0A2175;
         }
         /* epsilon_link_color */
         a:hover,
         a:focus {
         color: #AC3100;
         }
         /* epsilon_link_hover_color */
         a:active {
         color: #6B7598;
         }
         /* epsilon_link_active_color */
         input[type="text"],
         input[type="password"],
         input[type="date"],
         input[type="datetime"],
         input[type="datetime-local"],
         input[type="month"],
         input[type="week"],
         input[type="email"],
         input[type="number"],
         input[type="search"],
         input[type="tel"],
         input[type="time"],
         input[type="url"],
         input[type="color"],
         textarea,
         select {
         color: rgba(0, 0, 0, 1);
         /* epsilon_text_color */
         }
         .ewf-btn,
         input[type="reset"],
         input[type="submit"],
         input[type="button"] {
         background-color: #C95A2E;
         }
         .ewf-btn:hover,
         input[type="reset"]:hover,
         input[type="submit"]:hover,
         input[type="button"]:hover,
         input[type="reset"]:focus,
         input[type="submit"]:focus,
         input[type="button"]:focus {
         background-color: #2C4088;
         }
         input[type="reset"]:active,
         input[type="submit"]:active,
         input[type="button"]:active {
         background-color: #C95A2E;
         }
         /* ==========================================================================
         =Footer
         ========================================================================== */
         #footer {
         background-color: #75A200;
         /* epsilon_footer_background */
         color: rgba(0, 0, 0, 1);
         /* epsilon_footer_text_color */
         }
         #footer-bottom {
         background-color: #6A9200;
         }
         #footer a {
         color: #011769;
         }
         /* epsilon_footer_link_color */
         #footer a:hover {
         color: #EFEFEF;
         }
         /* epsilon_footer_link_hover_color */
         #footer a:focus {
         color: #EFEFEF;
         }
         /* epsilon_footer_link_hover_color */
         #footer a:active {
         color: #A2A2A2;
         }
         /* epsilon_footer_link_active_color */
         #footer h1,
         #footer h2,
         #footer h3,
         #footer h4,
         #footer h5,
         #footer h6 {
         color: #011769;
         }
         #footer p {
         color: rgba(0, 0, 0, 1);
         }
         /* epsilon_footer_text_color */
         .contact-form {
         background-color: #0377bb;
         /* epsilon_footer_contact_background */
         }
         #footer .widget .widget-title,
         #footer .widget .widget-title a {
         color: #011769;
         /* epsilon_footer_title_color */
         }
         /* ==========================================================================
         =Blog
         ========================================================================== */
         #back-to-top {
         background-color: #2C4088;
         }
         #back-to-top:hover {
         background-color: #2C4088;
         }
         #back-to-top:hover i {
         color: #ffffff;
         }
         /*
         .sticky .post-thumbnail:after {
         color: #2C4088;
         }
         */
         .post {
         color: rgba(0, 0, 0, 1);
         }
         .post-title a {
         color: 011769;
         }
         .post-title a:focus,
         .post-title a:hover {
         color: #AC3100;
         }
         .post-title a:active {
         color: #6B7598;
         }
         .post-meta a {
         color: 011769;
         }
         .post-meta a:focus,
         .post-meta a:hover {
         color: #AC3100;
         }
         .post-meta a:active {
         color: #6B7598;
         }
         .tags-links {
         color: 011769;
         }
         .tags-links a {
         border-color: #0A2175;
         color: #0A2175;
         }
         .tags-links a:hover {
         color: #AC3100;
         border-color: #AC3100;
         }
         .more-link:before {
         border-top-color: #0A2175;
         }
         .more-link:hover:before {
         border-top-color: #AC3100;
         }
         .posted-on {
         color: 011769;
         }
         .post-format {
         color: #2C4088;
         }
         .pagination .page-numbers.current {
         border-bottom-color: #2C4088;
         }
         .pagination .page-numbers:hover {
         color: #2C4088;
         }
         .pagination .page-numbers:active {
         color: $#2C4088;
         }
         .pagination .prev.page-numbers,
         .pagination .next.page-numbers,
         .pagination .prev.page-numbers:active,
         .pagination .next.page-numbers:active {
         border: 1px solid rgba(0, 0, 0, 1);
         color: rgba(0, 0, 0, 1);
         }
         .pagination .prev.page-numbers:focus,
         .pagination .prev.page-numbers:hover,
         .pagination .next.page-numbers:focus,
         .pagination .next.page-numbers:hover {
         border: 1px solid #2C4088;
         background: #2C4088;
         color: #fff;
         }
         .page-links {
         color: 011769;
         }
         .post-navigation {
         border-top-color: #ebebeb;
         }
         .post-navigation .nav-subtitle {
         color: 011769;
         }
         .author-bio {
         border-color: #ebebeb;
         background-color: $background-color;
         }
         .author-bio-social a {
         color: 011769;
         }
         .no-comments,
         .comment-awaiting-moderation {
         color: 011769;
         }
         .comment-metadata>a {
         color: rgba(0, 0, 0, 1);
         }
         .comment .comment-body {
         border-top-color: #ebebeb;
         }
         #comment-nav-above {
         border-bottom-color: #ebebeb;
         }
         #comment-nav-below {
         border-top: 1px solid #ebebeb;
         }
         .widget_archive li {
         border-bottom-color: #ebebeb;
         }
         .widget .widget-title,
         .widget .widget-title a {
         color: #C95A2E;
         }
         #wp-calendar caption {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: 011769;
         }
         #wp-calendar th {
         color: 011769;
         }
         #wp-calendar tbody td a {
         background-color: #2C4088;
         color: #ffffff;
         }
         #wp-calendar tbody td a:hover {
         background-color: #2C4088;
         }
         #wp-calendar #prev a:focus,
         #wp-calendar #prev a:hover {
         background-color: #2C4088;
         border-color: #2C4088;
         }
         #wp-calendar #prev a:before {
         color: #2C4088;
         }
         #wp-calendar #prev a:hover:before {
         color: #fff;
         }
         #wp-calendar #next a:before {
         color: #2C4088;
         }
         #wp-calendar #next a:hover:before {
         color: #fff;
         }
         #wp-calendar #next a:focus,
         #wp-calendar #next a:hover {
         background-color: #2C4088;
         border-color: #2C4088;
         }
         .widget_categories li {
         border-bottom-color: #ebebeb;
         }
         .widget_recent_entries ul li {
         border-bottom-color: #ebebeb;
         }
         .widget_rss .rss-date,
         .widget_rss cite {
         color: rgba(0, 0, 0, 1);
         }
         .widget_search .search-submit {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: #2C4088;
         }
         .widget_search .search-submit:hover,
         .widget_search .search-submit:focus {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: #AC3100;
         }
         .widget_search .search-submit:active {
         background-color: #ffffff;
         border-color: #ebebeb;
         color: 011769;
         }
         .widget_tag_cloud a {
         border-color: #0A2175;
         color: #0A2175;
         }
         .widget_tag_cloud a:hover {
         color: #AC3100;
         border-color: #AC3100;
         }
         .widget a {
         color: #0A2175;
         }
         .widget a:hover,
         .widget a:focus {
         color: #AC3100;
         }
         .widget a:active {
         color: #6B7598;
         }
      </style>
      <link rel='stylesheet' id='tiko-style-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/css/tiko-style.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='wpdevelop-bts-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap.css?ver=3.3.5.1' type='text/css' media='all' />
      <link rel='stylesheet' id='wpdevelop-bts-theme-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap-theme.css?ver=3.3.5.1' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-client-pages-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/client.css?ver=8.4.6' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-admin-timeline-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/timeline.css?ver=8.4.6' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-calendar-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/calendar.css?ver=8.4.6' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-calendar-skin-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/skins/traditional.css?ver=8.4.6' type='text/css' media='all' />
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var wpbc_global1 = {"wpbc_ajaxurl":"http:\/\/localhost\/exergieafrique.dev\/wp-admin\/admin-ajax.php","wpdev_bk_plugin_url":"http:\/\/localhost\/exergieafrique.dev\/wp-content\/plugins\/booking","wpdev_bk_today":"[2019,6,6,21,48]","visible_booking_id_on_page":"[]","booking_max_monthes_in_calendar":"1y","user_unavilable_days":"[999]","wpdev_bk_edit_id_hash":"","wpdev_bk_plugin_filename":"wpdev-booking.php","bk_days_selection_mode":"multiple","wpdev_bk_personal":"0","block_some_dates_from_today":"0","message_verif_requred":"This field is required","message_verif_requred_for_check_box":"This checkbox must be checked","message_verif_requred_for_radio_box":"At least one option must be selected","message_verif_emeil":"Incorrect email field","message_verif_same_emeil":"Your emails do not match","message_verif_selectdts":"Please, select booking date(s) at Calendar.","parent_booking_resources":"[]","new_booking_title":"Thank you for your online booking.  We will send confirmation of your booking as soon as possible.","new_booking_title_time":"7000","type_of_thank_you_message":"message","thank_you_page_URL":"http:\/\/localhost\/exergieafrique.dev\/thank-you","is_am_pm_inside_time":"true","is_booking_used_check_in_out_time":"false","wpbc_active_locale":"en_US","wpbc_message_processing":"Processing","wpbc_message_deleting":"Deleting","wpbc_message_updating":"Updating","wpbc_message_saving":"Saving","message_checkinouttime_error":"Error! Please reset your check-in\/check-out dates above.","message_starttime_error":"Start Time is invalid. The date or time may be booked, or already in the past! Please choose another date or time.","message_endtime_error":"End Time is invalid. The date or time may be booked, or already in the past. The End Time may also be earlier that the start time, if only 1 day was selected! Please choose another date or time.","message_rangetime_error":"The time(s) may be booked, or already in the past!","message_durationtime_error":"The time(s) may be booked, or already in the past!","bk_highlight_timeslot_word":"Times:"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/wpbc_vars.js?ver=8.4.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/assets/libs/bootstrap/js/bootstrap.js?ver=3.3.5.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/datepick/jquery.datepick.js?ver=1.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/client.js?ver=8.4.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/wpbc_times.js?ver=8.4.6'></script>
      <link rel='https://api.w.org/' href='http://localhost/exergieafrique.dev/index.php?rest_route=/' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/exergieafrique.dev/xmlrpc.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/exergieafrique.dev/wp-includes/wlwmanifest.xml" />
      <link rel="canonical" href="http://localhost/exergieafrique.dev/?page_id=35" />
      <link rel='shortlink' href='http://localhost/exergieafrique.dev/?p=35' />
      <link rel="alternate" type="application/json+oembed" href="http://localhost/exergieafrique.dev/index.php?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Flocalhost%2Fexergieafrique.dev%2F%3Fpage_id%3D35" />
      <meta name="tec-api-version" content="v1">
      <meta name="tec-api-origin" content="http://localhost/exergieafrique.dev">
      <link rel="https://theeventscalendar.com/" href="http://localhost/exergieafrique.dev/index.php?rest_route=/tribe/events/v1/" />
      <style type="text/css" media="print">#wpadminbar { display:none; }</style>
      <style type="text/css" media="screen">
         html { margin-top: 32px !important; }
         * html body { margin-top: 32px !important; }
         @media screen and ( max-width: 782px ) {
         html { margin-top: 46px !important; }
         * html body { margin-top: 46px !important; }
         }
      </style>
      <link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1-150x150.png" sizes="32x32" />
      <link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" />
      <meta name="msapplication-TileImage" content="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" />
   </head>
   <body class="page-template-default page page-id-35 logged-in admin-bar no-customize-support wp-custom-logo tribe-no-js sticky-header">
      <div id="header" class="exergie-classic header--no-shadow header--has-sticky-logo">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div id="logo">
                     <a href="http://localhost/exergieafrique.dev/" class="custom-logo-link" rel="home" itemprop="url"><img width="276" height="60" src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/EXERGIE-afriqueVsoft_rectangle60-1.png" class="custom-logo" alt="Exergie Afrique" itemprop="logo" srcset="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/EXERGIE-afriqueVsoft_rectangle60-1.png 276w, http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/EXERGIE-afriqueVsoft_rectangle60-1-275x60.png 275w" sizes="(max-width: 276px) 100vw, 276px" /></a>
                     <a href="http://localhost/exergieafrique.dev/" class="custom-logo-link--sticky" rel="home" itemprop="url"><img class="custom-logo--sticky" src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/EXERGIE-afriqueVsoft_rectangle60-1.png" itemprop="logo" width="276" height="60" alt="Exergie Afrique" /></a>
                  </div>
                  <!-- end #logo -->
                  <div class="exergie-menu-icon">
                     <div class="exergie-navicon"></div>
                  </div>
                  <div id="custom_html-5" class="widget_text widget widget_custom_html">
                     <div class="textwidget custom-html-widget">
                        <div class="header-social-icons">
                           <a target="_blank" href="http://www.twitter.com" title="Twitter">
                           <i class="fa fa-twitter">
                           </i>
                           </a>
                           <a target="_blank" href="http://www.facebook.com" title="Facebook">
                           <i class="fa fa-facebook">
                           </i>
                           </a>
                           <a target="_blank" href="https://www.linkedin.com/" title="Linkedin ">
                           <i class="fa fa-linkedin">
                           </i>
                           </a>
                        </div>
                     </div>
                  </div>
                  <ul id="menu" class="exergie-menu fixed">
                     <li id="menu-item-567" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-567"><a href="http://localhost/exergieafrique.dev/">Accueil</a></li>
                     <li id="menu-item-1018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1018"><a href="http://localhost/exergieafrique.dev/?page_id=74">VSOFT</a></li>
                     <li id="menu-item-760" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-760">
                        <a href="http://localhost/exergieafrique.dev/?page_id=64">Nos domaines d’expertise</a>
                        <ul class="sub-menu">
                           <li id="menu-item-761" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-761"><a href="http://localhost/exergieafrique.dev/?page_id=74">VSOFT: Analyse de facture</a></li>
                           <li id="menu-item-762" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-762"><a href="http://localhost/exergieafrique.dev/?page_id=44">Les formations</a></li>
                           <li id="menu-item-767" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-767"><a href="http://localhost/exergieafrique.dev/?page_id=43">Audit énergétique</a></li>
                        </ul>
                     </li>
                     <li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-35 current_page_item menu-item-73"><a href="http://localhost/exergieafrique.dev/?page_id=35" aria-current="page">À propos</a></li>
                     <li id="menu-item-565" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-565"><a href="http://localhost/exergieafrique.dev/?page_id=287">Contact</a></li>
                  </ul>
               </div>
            </div>
            <!-- end .row -->
         </div>
         <!-- end .container -->
      </div>
      <!-- #header -->
      <div id="wrap">
         <div id="content" "page-content">
            <div class="container page-container">
               <div class="row">
                  <!-- <div class="col-md-1"></div>
                     <div class="col-md-10">
                     <div class="intro-item">
                     <h4>Qui sommes nous ?</h4>
                     </div>
                     </div>
                     <div class="col-md-1"></div> -->
               </div>
               <div class="row">
                  <div class="col-sm-4 page-sidebar-col">
                     <!-- /// SIDEBAR CONTENT  /////////////////////////////////////////////////////////////////////////////////// -->
                     <div id="img-page-" class="page-image">
                        <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/énergies-renouvelables.jpg" alt="Exergie Afrique"/>
                     </div>
                     <div id="shortcodes-ultimate-2" class="widget shortcodes-ultimate">
                        <div class="textwidget">
                           <div class="su-divider su-divider-style-default" style="margin:40px 0;border-width:3px;border-color:#A2A2A2"></div>
                        </div>
                     </div>
                     <div id="nav_menu-2" class="widget widget_nav_menu">
                        <div class="menu-latéral-container">
                           <ul id="menu-latéral" class="menu">
                              <li id="menu-item-1153" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1153"><a href="http://localhost/exergieafrique.dev/?page_id=35"><span>Présentation</span></a></li>
                              <li id="menu-item-1154" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1154"><a href="http://localhost/exergieafrique.dev/?page_id=287"><span>Le Président</span></a></li>
                              <li id="menu-item-1155" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1155"><a href="#"><span>Missions & Objectifs</span></a></li>
                              <li id="menu-item-1156" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1156"><a href="#"><span>Activités</span></a></li>
                              <li id="menu-item-1157" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1157"><a href="#"><span>Organigramme</span></a></li>
                           </ul>
                        </div>
                     </div>
                     <div id="shortcodes-ultimate-3" class="widget shortcodes-ultimate">
                        <div class="textwidget">
                           <div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:3px;border-color:#A2A2A2"></div>
                        </div>
                     </div>
                     <div id="mtphr-dnt-widget-3" class="widget mtphr-dnt-widget">
                        <h5 class="widget-title">Actualités</h5>
                        <div id="mtphr-dnt-1161-widget" class="mtphr-dnt mtphr-dnt-1161 mtphr-dnt-default mtphr-dnt-rotate mtphr-dnt-rotate-slide_up">
                           <div class="mtphr-dnt-wrapper mtphr-dnt-clearfix">
                              <div class="mtphr-dnt-tick-container">
                                 <div class="mtphr-dnt-tick-contents">
                                    <div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1076" target="_self">Formation : Contrôle des factures d&#8217;éléctricité dans le secteur public</a></div>
                                    <div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1082" target="_self">Séminaire sur l&#8217;Efficacité énergétique du 17 au 22 octobre 2019 à Dakar (Sénégal)</a></div>
                                    <div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1078" target="_self">Session facturation</a></div>
                                    <div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1068" target="_self">Nouveautés et applications au service de la performance énergétique </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="shortcodes-ultimate-4" class="widget shortcodes-ultimate">
                        <div class="textwidget">
                           <div class="su-divider su-divider-style-double" style="margin:30px 0;border-width:6px;border-color:#A2A2A2"><a href="#" style="color:#A2A2A2">En haut</a></div>
                        </div>
                     </div>
                     <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                  </div>
                  <div class="col-sm-8">
                     <!-- /// MAIN CONTENT  ////////////////////////////////////////////////////////////////////////////////////// -->
                     <article id="post-35" class="post-35 page type-page status-publish has-post-thumbnail hentry">
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="post-content">
                                 <figure class="wp-block-pullquote" style="border-color:#2c4088">
                                    <blockquote class="has-text-color" style="color:#95be2b">
                                       <p>Nous faisons de l’énergie durable un business rentable pour nos clients.</p>
                                    </blockquote>
                                 </figure>
                                 <p class="has-drop-cap"><strong>Exergie
                                    Afrique</strong>
                                    est une entreprise d’expert-conseil spécialisée en énergie
                                    durable (efficacité énergétique et énergie durable). Nous
                                    accompagnons les États, les entreprises, les organisations
                                    régionales de développement et les institutions financières, dans
                                    le cercle vertueux du développement durable grâce à la maîtrise
                                    de l’énergie. Nos produits et services à forte valeur ajoutée,
                                    offrent des économies financières substantielles avec des
                                    rentabilités élevées. 
                                 </p>
                                 <figure class="wp-block-pullquote is-style-default" style="border-color:#2c4088">
                                    <blockquote class="has-text-color" style="color:#95be2b">
                                       <p>le prix de l’électricité représente environ plus 10% du budget des ménages et en moyenne plus de 30% du coût de production en entreprise</p>
                                    </blockquote>
                                 </figure>
                                 <h3>Le contexte énergétique en Afrique</h3>
                                 <p style="text-align:left">L’efficacité énergétique et les énergies renouvelables constituent les moyens privilégiés pour la maîtrise de l’énergie et partant, pour la lutte contre le changement climatique. Sous l’impulsion des organismes internationaux et des gouvernements, l’efficacité énergétique est intégrée dans les codes de construction ; des efforts considérables sont faits tous les jours pour améliorer le rendement énergétique des équipements&nbsp;; les énergies renouvelables deviennent plus accessibles grâce à la réduction progressive des coûts et à la disponibilité de fonds spécialisés ; des fonds à taux préférentiels et des incitatifs sont de plus en plus accessibles ; des systèmes et procédures de gestion durable de l’énergie sont implantés dans les entreprises en vue d’améliorer les rendements de la gestion de l’énergie.</p>
                                 <div class="wp-block-image">
                                    <figure class="aligncenter is-resized">
                                       <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/figue1.svg.png" alt="" class="wp-image-1256" width="537" height="333" srcset="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/figue1.svg.png 600w, http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/figue1.svg-300x186.png 300w" sizes="(max-width: 537px) 100vw, 537px" />
                                       <figcaption>Contexte énergétique des pays Africains</figcaption>
                                    </figure>
                                 </div>
                                 <p>Dans
                                    un tel contexte, le prix de l’électricité représente environ
                                    plus 10% du budget des ménages et en moyenne plus de 30% du coût de
                                    production en entreprise ; ce qui constitue un réel frein au
                                    développement économique et social. Cette situation est aggravée
                                    par le choix de ressources énergétiques primaires, le mode de
                                    production et d’utilisation de l’énergie finale, peu regardants
                                    sur les problématiques du développement durable.
                                 </p>
                                 <h4>Nos programmes</h4>
                                 <ul class="su-subpages">
                                    <li class="page_item page-item-74"><a href="http://localhost/exergieafrique.dev/?page_id=74">VSOFT</a></li>
                                    <li class="page_item page-item-44 page_item_has_children">
                                       <a href="http://localhost/exergieafrique.dev/?page_id=44">PFEER: Programme de Formation en Efficacité Énergétique et Énergie Renouvelable</a>
                                       <ul class='children'>
                                          <li class="page_item page-item-45"><a href="http://localhost/exergieafrique.dev/?page_id=45">Thème 1 : Mise en place du PSCOFE en entreprise et dans les États</a></li>
                                          <li class="page_item page-item-46"><a href="http://localhost/exergieafrique.dev/?page_id=46">Thème 2 : Mise en place d’un programme d’efficacité énergétique (PGEER) en entreprise</a></li>
                                          <li class="page_item page-item-47"><a href="http://localhost/exergieafrique.dev/?page_id=47">Thème 3 : Réalisation et mise en œuvre d’un diagnostic énergétique sur mesure</a></li>
                                          <li class="page_item page-item-42"><a href="http://localhost/exergieafrique.dev/?page_id=42">Thème 4 : Comment accroitre les rendements grâce à la comptabilité énergétique</a></li>
                                       </ul>
                                    </li>
                                    <li class="page_item page-item-43"><a href="http://localhost/exergieafrique.dev/?page_id=43">PGEER: Programme Global d’Efficacité Énergétique et d’Énergie Renouvelable</a></li>
                                 </ul>
                              </div>
                              <!-- .post-content -->
                           </div>
                        </div>
                     </article>
                     <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                  </div>
               </div>
            </div>
         </div>
         <div id="footer">
            <div id="footer-bottom" class="row footer-sub">
               <!-- /// FOOTER-BOTTOM  ////////////////////////////////////////////////////////////////////////////////////////////// -->
               <div class="container">
                  <div class="row">
                     <div id="footer-bottom-widget-area-1" class="col-sm-6 ol-xs-12">
                        <div class="pull-rigth">
                           Theme by: <a href="https://home.tiko.ci/">Tiko Themes</a>. All rights reserved &copy 2019.			 
                        </div>
                     </div>
                     <!-- end .col -->
                     <div id="footer-bottom-widget-area-2" class="col-sm-6 col-xs-12">
                        <div id="custom_html-7" class="widget_text widget widget_custom_html">
                           <div class="textwidget custom-html-widget">
                              <div class="footer-social-icons">
                                 <a target="_blank" href="http://www.twitter.com" title="Twitter">
                                 <i class="fa fa-twitter">
                                 </i>
                                 </a>
                                 <a target="_blank" href="http://www.facebook.com" title="Facebook">
                                 <i class="fa fa-facebook">
                                 </i>
                                 </a>
                                 <a target="_blank" href="https://www.linkedin.com/" title="Linkedin ">
                                 <i class="fa fa-linkedin">
                                 </i>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <ul id="menu-menu-de-pied-de-page" class="nav">
                           <li id="menu-item-897" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-897"><a href="#">Protection des données</a></li>
                           <li id="menu-item-898" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-898"><a href="#">Carte du site</a></li>
                        </ul>
                     </div>
                     <!-- end .col -->
                  </div>
                  <!-- end .row -->
               </div>
               <!-- end .container -->
               <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
            </div>
            <!-- end #footer-bottom -->
         </div>
      </div>
      <a id="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
      <script>
         ( function ( body ) {
         	'use strict';
         	body.className = body.className.replace( /\btribe-no-js\b/, 'tribe-js' );
         } )( document.body );
      </script>
      <script> /* <![CDATA[ */var tribe_l10n_datatables = {"aria":{"sort_ascending":": activate to sort column ascending","sort_descending":": activate to sort column descending"},"length_menu":"Show _MENU_ entries","empty_table":"No data available in table","info":"Showing _START_ to _END_ of _TOTAL_ entries","info_empty":"Showing 0 to 0 of 0 entries","info_filtered":"(filtered from _MAX_ total entries)","zero_records":"No matching records found","search":"Search:","all_selected_text":"All items on this page were selected. ","select_all_link":"Select all pages","clear_selection":"Clear Selection.","pagination":{"all":"All","next":"Next","previous":"Previous"},"select":{"rows":{"0":"","_":": Selected %d rows","1":": Selected 1 row"}},"datepicker":{"dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesMin":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Prev","currentText":"Today","closeText":"Done","today":"Today","clear":"Clear"}};var tribe_system_info = {"sysinfo_optin_nonce":"be0f458a22","clipboard_btn_text":"Copy to clipboard","clipboard_copied_text":"System info copied","clipboard_fail_text":"Press \"Cmd + C\" to copy"};/* ]]> */ </script>
      <link rel='stylesheet' id='su-shortcodes-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/shortcodes-ultimate/includes/css/shortcodes.css?ver=5.3.0' type='text/css' media='all' />
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/admin-bar.min.js?ver=5.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var wpcf7 = {"apiSettings":{"root":"http:\/\/localhost\/exergieafrique.dev\/index.php?rest_route=\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/jquery.touchSwipe.min.js?ver=2.2.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/jquery.easing.js?ver=1.4.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/imagesloaded.pkgd.min.js?ver=4.1.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var mtphr_dnt_vars = {"is_rtl":""};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/ditty-news-ticker.min.js?ver=1559147953'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/viewport/viewport.js?ver=1.0.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/vendors/plyr/plyr.js?ver=1.0.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/js/exergie.js?ver=1.0.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/js/tiko-func.js?ver=1.0.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie/assets/js/main.js?ver=1.0.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/wp-embed.min.js?ver=5.1.1'></script>
      <script>
         jQuery( document ).ready( function($) {
         				$( '#mtphr-dnt-1161-widget' ).ditty_news_ticker({
         		id : '1161-widget',
         		type : 'rotate',
         		scroll_direction : 'up',
         		scroll_speed : 0,
         		scroll_pause : 0,
         		scroll_spacing : 40,
         		scroll_init : 1,
         		rotate_type : 'slide_up',
         		auto_rotate : 1,
         		rotate_delay : 8,
         		rotate_pause : 1,
         		rotate_speed : 10,
         		rotate_ease : 'easeInOutQuint',
         		nav_reverse : 1,
         		disable_touchswipe : 1,
         		offset : 100,
         		after_load : function( $ticker ) {
         								},
         		before_change : function( $ticker ) {
         								},
         		after_change : function( $ticker ) {
         								}
         	});
          			});
      </script>
      <!--[if lte IE 8]>
      <script type="text/javascript">
         document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
      </script>
      <![endif]-->
      <!--[if gte IE 9]><!-->
      <script type="text/javascript">
         (function() {
         	var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');
         
         			request = true;
         
         	b[c] = b[c].replace( rcs, ' ' );
         	// The customizer requires postMessage and CORS (if the site is cross domain)
         	b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
         }());
      </script>
      <!--<![endif]-->
      <div id="wpadminbar" class="nojq nojs">
         <a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
         <div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar">
            <ul id='wp-admin-bar-root-default' class="ab-top-menu">
               <li id='wp-admin-bar-wp-logo' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/about.php'><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-wp-logo-default' class="ab-submenu">
                        <li id='wp-admin-bar-about'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/about.php'>About WordPress</a></li>
                     </ul>
                     <ul id='wp-admin-bar-wp-logo-external' class="ab-sub-secondary ab-submenu">
                        <li id='wp-admin-bar-wporg'><a class='ab-item' href='https://wordpress.org/'>WordPress.org</a></li>
                        <li id='wp-admin-bar-documentation'><a class='ab-item' href='https://codex.wordpress.org/'>Documentation</a></li>
                        <li id='wp-admin-bar-support-forums'><a class='ab-item' href='https://wordpress.org/support/'>Support Forums</a></li>
                        <li id='wp-admin-bar-feedback'><a class='ab-item' href='https://wordpress.org/support/forum/requests-and-feedback'>Feedback</a></li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-site-name' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/'>Exergie Afrique</a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-site-name-default' class="ab-submenu">
                        <li id='wp-admin-bar-dashboard'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/'>Dashboard</a></li>
                     </ul>
                     <ul id='wp-admin-bar-appearance' class="ab-submenu">
                        <li id='wp-admin-bar-themes'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/themes.php'>Themes</a></li>
                        <li id='wp-admin-bar-widgets'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/widgets.php'>Widgets</a></li>
                        <li id='wp-admin-bar-menus'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/nav-menus.php'>Menus</a></li>
                        <li id='wp-admin-bar-header' class="hide-if-customize"><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/themes.php?page=custom-header'>Header</a></li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-customize' class="hide-if-no-customize"><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/customize.php?url=http%3A%2F%2Flocalhost%2Fexergieafrique.dev%2F%3Fpage_id%3D35'>Customize</a></li>
               <li id='wp-admin-bar-updates'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/update-core.php' title='3 Plugin Updates, Translation Updates'><span class="ab-icon"></span><span class="ab-label">4</span><span class="screen-reader-text">3 Plugin Updates, Translation Updates</span></a></li>
               <li id='wp-admin-bar-comments'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit-comments.php'><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a></li>
               <li id='wp-admin-bar-bar_wpbc' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc'>Booking Calendar</a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-bar_wpbc-default' class="ab-submenu">
                        <li id='wp-admin-bar-bar_wpbc_calendar_overview'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc&#038;view_mode=vm_calendar'>Calendar Overview</a></li>
                        <li id='wp-admin-bar-bar_wpbc_booking_listing'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc&#038;view_mode=vm_listing'>Booking Listing</a></li>
                        <li id='wp-admin-bar-bar_wpbc_new'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-new'>Add booking</a></li>
                        <li id='wp-admin-bar-bar_wpbc_settings' class="menupop">
                           <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings'>Settings</a>
                           <div class="ab-sub-wrapper">
                              <ul id='wp-admin-bar-bar_wpbc_settings-default' class="ab-submenu">
                                 <li id='wp-admin-bar-bar_wpbc_settings_form'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings&#038;tab=form'>Form</a></li>
                                 <li id='wp-admin-bar-bar_wpbc_settings_email'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings&#038;tab=email'>Emails</a></li>
                                 <li id='wp-admin-bar-bar_wpbc_settings_sync'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings&#038;tab=sync'>Sync</a></li>
                              </ul>
                           </div>
                        </li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-new-content' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/post-new.php'><span class="ab-icon"></span><span class="ab-label">New</span></a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-new-content-default' class="ab-submenu">
                        <li id='wp-admin-bar-new-post'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php'>Post</a></li>
                        <li id='wp-admin-bar-new-media'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/media-new.php'>Media</a></li>
                        <li id='wp-admin-bar-new-page'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=page'>Page</a></li>
                        <li id='wp-admin-bar-new-ditty_news_ticker'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=ditty_news_ticker'>News Ticker</a></li>
                        <li id='wp-admin-bar-new-tribe_events'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=tribe_events'>Event</a></li>
                        <li id='wp-admin-bar-new-user'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/user-new.php'>User</a></li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-edit'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post.php?post=35&#038;action=edit'>Edit Page</a></li>
               <li id='wp-admin-bar-maintenance_options'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=maintenance' title='Maintenance is Off'>Maintenance is Off</a></li>
               <li id='wp-admin-bar-tribe-events' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev?post_type=tribe_events'><span class="ab-icon dashicons-before dashicons-calendar"></span>Events</a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-tribe-events-group' class="ab-submenu">
                        <li id='wp-admin-bar-tribe-events-view-calendar'><a class='ab-item' href='http://localhost/exergieafrique.dev?post_type=tribe_events'>View Calendar</a></li>
                        <li id='wp-admin-bar-tribe-events-add-event'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=tribe_events'>Add Event</a></li>
                        <li id='wp-admin-bar-tribe-events-edit-events'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?post_type=tribe_events'>Edit Events</a></li>
                     </ul>
                     <div id="wp-admin-bar-tribe-events-add-ons-group-container" class="ab-group-container">
                        <ul id='wp-admin-bar-tribe-events-import-group' class="ab-submenu">
                           <li id='wp-admin-bar-tribe-events-import' class="menupop">
                              <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/edit.php?post_type=tribe_events&#038;page=aggregator'>Import</a>
                              <div class="ab-sub-wrapper">
                                 <ul id='wp-admin-bar-tribe-events-import-default' class="ab-submenu">
                                    <li id='wp-admin-bar-tribe-aggregator-import-csv'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?page=aggregator&#038;post_type=tribe_events&#038;ea-origin=csv'>CSV File</a></li>
                                 </ul>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <ul id='wp-admin-bar-tribe-events-settings-group' class="ab-submenu">
                        <li id='wp-admin-bar-tribe-events-settings'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?page=tribe-common&#038;post_type=tribe_events'>Settings</a></li>
                        <li id='wp-admin-bar-tribe-events-help'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?post_type=tribe_events&#038;page=tribe-help'>Help</a></li>
                        <li id='wp-admin-bar-tribe-events-app-shop'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?page=tribe-app-shop&#038;post_type=tribe_events'>Event Add-Ons</a></li>
                     </ul>
                  </div>
               </li>
            </ul>
            <ul id='wp-admin-bar-top-secondary' class="ab-top-secondary ab-top-menu">
               <li id='wp-admin-bar-search' class="admin-bar-search">
                  <div class="ab-item ab-empty-item" tabindex="-1">
                     <form action="http://localhost/exergieafrique.dev/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form>
                  </div>
               </li>
               <li id='wp-admin-bar-my-account' class="menupop with-avatar">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/profile.php'>Howdy, <span class="display-name">Anémone Kouadio</span><img alt='' src='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=26&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=52&#038;d=mm&#038;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-user-actions' class="ab-submenu">
                        <li id='wp-admin-bar-user-info'><a class='ab-item' tabindex="-1" href='http://localhost/exergieafrique.dev/wp-admin/profile.php'><img alt='' src='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=64&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=128&#038;d=mm&#038;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>Anémone Kouadio</span><span class='username'>anemone</span></a></li>
                        <li id='wp-admin-bar-edit-profile'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/profile.php'>Edit My Profile</a></li>
                        <li id='wp-admin-bar-logout'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-login.php?action=logout&#038;_wpnonce=623c6988ca'>Log Out</a></li>
                     </ul>
                  </div>
               </li>
            </ul>
         </div>
         <a class="screen-reader-shortcut" href="http://localhost/exergieafrique.dev/wp-login.php?action=logout&#038;_wpnonce=623c6988ca">Log Out</a>
      </div>
   </body>
</html>

