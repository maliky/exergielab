<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Exergie Afrique &#8211; Maîtriser l&#039;ENERGIE pour un développement plus durable</title>
      <meta name='robots' content='noindex,follow' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Feed" href="http://localhost/exergieafrique.dev/?feed=rss2" />
      <link rel="alternate" type="application/rss+xml" title="Exergie Afrique &raquo; Comments Feed" href="http://localhost/exergieafrique.dev/?feed=comments-rss2" />
      <link rel="alternate" type="text/calendar" title="Exergie Afrique &raquo; iCal Feed" href="http://localhost/exergieafrique.dev?post_type=tribe_events&#038;ical=1" />
      <link href='http://fonts.googleapis.com/css?family=Advent Pro' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Stardos Stencil' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Tinos' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/localhost\/exergieafrique.dev\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.1"}};
         !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='dashicons-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/dashicons.min.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='admin-bar-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/admin-bar.min.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tribe-tooltip-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/the-events-calendar/common/src/resources/css/tooltip.min.css?ver=4.9.9' type='text/css' media='all' />
      <link rel='stylesheet' id='tribe-events-admin-menu-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/the-events-calendar/src/resources/css/admin-menu.min.css?ver=4.9.2' type='text/css' media='all' />
      <link rel='stylesheet' id='elusive-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/vendor/codeinwp/icon-picker/css/types/elusive.min.css?ver=2.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/vendor/codeinwp/icon-picker/css/types/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
      <link rel='stylesheet' id='foundation-icons-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/vendor/codeinwp/icon-picker/css/types/foundation-icons.min.css?ver=3.0' type='text/css' media='all' />
      <link rel='stylesheet' id='menu-icons-extra-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/menu-icons/css/extra.min.css?ver=0.11.5' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-block-library-css'  href='http://localhost/exergieafrique.dev/wp-includes/css/dist/block-library/style.min.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3' type='text/css' media='all' />
      <link rel='stylesheet' id='ditty-news-ticker-font-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/libs/fontastic/styles.css?ver=2.2.1' type='text/css' media='all' />
      <link rel='stylesheet' id='ditty-news-ticker-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/css/style.css?ver=1559147953' type='text/css' media='all' />
      <link rel='stylesheet' id='widgetopts-styles-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/widget-options/assets/css/widget-options.css' type='text/css' media='all' />
      <link rel='stylesheet' id='exergie-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/style.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='ion-icons-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/vendors/ionicons/ion.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='exergie-main-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/css/style-exergie.css?ver=1.0.7' type='text/css' media='all' />
      <style id='exergie-main-inline-css' type='text/css'>
         [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
         [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style>
      <link rel='stylesheet' id='exergie-style-overrides-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/css/overrides.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tiko-style-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/css/tiko-style.css?ver=5.1.1' type='text/css' media='all' />
      <link rel='stylesheet' id='wpdevelop-bts-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap.css?ver=3.3.5.1' type='text/css' media='all' />
      <link rel='stylesheet' id='wpdevelop-bts-theme-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap-theme.css?ver=3.3.5.1' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-client-pages-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/client.css?ver=8.4.6' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-admin-timeline-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/timeline.css?ver=8.4.6' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-calendar-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/calendar.css?ver=8.4.6' type='text/css' media='all' />
      <link rel='stylesheet' id='wpbc-calendar-skin-css'  href='http://localhost/exergieafrique.dev/wp-content/plugins/booking/css/skins/traditional.css?ver=8.4.6' type='text/css' media='all' />
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var wpbc_global1 = {"wpbc_ajaxurl":"http:\/\/localhost\/exergieafrique.dev\/wp-admin\/admin-ajax.php","wpdev_bk_plugin_url":"http:\/\/localhost\/exergieafrique.dev\/wp-content\/plugins\/booking","wpdev_bk_today":"[2019,6,11,11,6]","visible_booking_id_on_page":"[]","booking_max_monthes_in_calendar":"1y","user_unavilable_days":"[999]","wpdev_bk_edit_id_hash":"","wpdev_bk_plugin_filename":"wpdev-booking.php","bk_days_selection_mode":"multiple","wpdev_bk_personal":"0","block_some_dates_from_today":"0","message_verif_requred":"This field is required","message_verif_requred_for_check_box":"This checkbox must be checked","message_verif_requred_for_radio_box":"At least one option must be selected","message_verif_emeil":"Incorrect email field","message_verif_same_emeil":"Your emails do not match","message_verif_selectdts":"Please, select booking date(s) at Calendar.","parent_booking_resources":"[]","new_booking_title":"Thank you for your online booking.  We will send confirmation of your booking as soon as possible.","new_booking_title_time":"7000","type_of_thank_you_message":"message","thank_you_page_URL":"http:\/\/localhost\/exergieafrique.dev\/thank-you","is_am_pm_inside_time":"true","is_booking_used_check_in_out_time":"false","wpbc_active_locale":"en_US","wpbc_message_processing":"Processing","wpbc_message_deleting":"Deleting","wpbc_message_updating":"Updating","wpbc_message_saving":"Saving","message_checkinouttime_error":"Error! Please reset your check-in\/check-out dates above.","message_starttime_error":"Start Time is invalid. The date or time may be booked, or already in the past! Please choose another date or time.","message_endtime_error":"End Time is invalid. The date or time may be booked, or already in the past. The End Time may also be earlier that the start time, if only 1 day was selected! Please choose another date or time.","message_rangetime_error":"The time(s) may be booked, or already in the past!","message_durationtime_error":"The time(s) may be booked, or already in the past!","bk_highlight_timeslot_word":"Times:"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/wpbc_vars.js?ver=8.4.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/assets/libs/bootstrap/js/bootstrap.js?ver=3.3.5.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/datepick/jquery.datepick.js?ver=1.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/client.js?ver=8.4.6'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/booking/js/wpbc_times.js?ver=8.4.6'></script>
      <link rel='https://api.w.org/' href='http://localhost/exergieafrique.dev/index.php?rest_route=/' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/exergieafrique.dev/xmlrpc.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/exergieafrique.dev/wp-includes/wlwmanifest.xml" />
      <link rel="canonical" href="http://localhost/exergieafrique.dev/" />
      <link rel='shortlink' href='http://localhost/exergieafrique.dev/' />
      <link rel="alternate" type="application/json+oembed" href="http://localhost/exergieafrique.dev/index.php?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Flocalhost%2Fexergieafrique.dev%2F" />
      <meta name="tec-api-version" content="v1">
      <meta name="tec-api-origin" content="http://localhost/exergieafrique.dev">
      <link rel="https://theeventscalendar.com/" href="http://localhost/exergieafrique.dev/index.php?rest_route=/tribe/events/v1/" />
      <style type="text/css" media="print">#wpadminbar { display:none; }</style>
      <style type="text/css" media="screen">
         html { margin-top: 32px !important; }
         * html body { margin-top: 32px !important; }
         @media screen and ( max-width: 782px ) {
         html { margin-top: 46px !important; }
         * html body { margin-top: 46px !important; }
         }
      </style>
      <link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1-150x150.png" sizes="32x32" />
      <link rel="icon" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" />
      <meta name="msapplication-TileImage" content="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/cropped-EXERGIE-afrique-SARL1_small-1.png" />
   </head>
   <body class="home page-template-default page page-id-258 logged-in admin-bar no-customize-support tribe-no-js sticky-header">
      <div id="header" class="exergie-classic header--no-shadow header--has-sticky-logo">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div id="logo">
                     <a href="http://localhost/exergieafrique.dev/" class="custom-logo-link" rel="home" itemprop="url">Exergie Afrique</a>
                  </div>
                  <!-- end #logo -->
                  <div class="exergie-menu-icon">
                     <div class="exergie-navicon"></div>
                  </div>
                  <div id="custom_html-5" class="widget_text widget widget_custom_html">
                     <div class="textwidget custom-html-widget">
                        <div class="header-social-icons">
                           <a target="_blank" href="http://www.twitter.com" title="Twitter">
                           <i class="fa fa-twitter">
                           </i>
                           </a>
                           <a target="_blank" href="http://www.facebook.com" title="Facebook">
                           <i class="fa fa-facebook">
                           </i>
                           </a>
                           <a target="_blank" href="https://www.linkedin.com/" title="Linkedin ">
                           <i class="fa fa-linkedin">
                           </i>
                           </a>
                        </div>
                     </div>
                  </div>
                  <ul id="menu" class="exergie-menu fixed">
                     <li id="menu-item-567" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-258 current_page_item menu-item-567"><a href="http://localhost/exergieafrique.dev/" aria-current="page">Accueil</a></li>
                     <li id="menu-item-1018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1018"><a href="http://localhost/exergieafrique.dev/?page_id=74">VSOFT</a></li>
                     <li id="menu-item-760" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-760">
                        <a href="http://localhost/exergieafrique.dev/?page_id=64">Nos domaines d’expertise</a>
                        <ul class="sub-menu">
                           <li id="menu-item-761" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-761"><a href="http://localhost/exergieafrique.dev/?page_id=74">VSOFT: Analyse de facture</a></li>
                           <li id="menu-item-762" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-762"><a href="http://localhost/exergieafrique.dev/?page_id=44">Les formations</a></li>
                           <li id="menu-item-767" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-767"><a href="http://localhost/exergieafrique.dev/?page_id=43">Audit énergétique</a></li>
                        </ul>
                     </li>
                     <li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="http://localhost/exergieafrique.dev/?page_id=35">À propos</a></li>
                     <li id="menu-item-565" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-565"><a href="http://localhost/exergieafrique.dev/?page_id=287">Contact</a></li>
                  </ul>
               </div>
            </div>
            <!-- end .row -->
         </div>
         <!-- end .container -->
      </div>
      <!-- #header -->
      <div id="wrap">
      <section data-customizer-section-id="exergie_repeatable_section" data-section="0">
         <div class="ewf-advanced-slider ewf-section-visible ewf-slider"
            data-slider-speed="8000"
            data-slider-autoplay="true"
            data-slides-shown="2"
            data-slides-scrolled="1"
            data-slider-loop="true"
            data-slider-enable-pager="false"
            data-slider-enable-controls="false">
            <ul class="ewf-slider__slides">
               <li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_Electricite_poste_source_x500.jpg);height:35vh;;">
                  <div class="ewf-slider-slide__overlay" style="background-color:rgba(0,0,0,.1)" ></div>
                  <div class="ewf-slider-slide__content ewf-valign--middle ewf-text-align--center">
                     <div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
                     </div>
                     <!-- end .ewf-slider-slide__content -->
                  </div>
               </li>
               <li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_formation_x500.png);height:35vh;;">
                  <div class="ewf-slider-slide__overlay" style="background-color:rgba(0,0,0,.1)" ></div>
                  <div class="ewf-slider-slide__content ewf-valign--middle ewf-text-align--center">
                     <div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
                     </div>
                     <!-- end .ewf-slider-slide__content -->
                  </div>
               </li>
               <li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_ampoule_courbe_x500.jpg);height:35vh;;">
                  <div class="ewf-slider-slide__overlay" style="background-color:rgba(0,0,0,.1)" ></div>
                  <div class="ewf-slider-slide__content ewf-valign--middle ewf-text-align--center">
                     <div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
                     </div>
                     <!-- end .ewf-slider-slide__content -->
                  </div>
               </li>
               <li style="background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_helmet-resting-on-solar-panel_x500.jpg);height:35vh;;">
                  <div class="ewf-slider-slide__overlay" style="background-color:rgba(0,0,0,.1)" ></div>
                  <div class="ewf-slider-slide__content ewf-valign--middle ewf-text-align--center">
                     <div class="ewf-slider-slide__content-wrap" style="max-width: 100%;">
                     </div>
                     <!-- end .ewf-slider-slide__content -->
                  </div>
               </li>
            </ul>
            <!-- end .slides -->
            <div class="ewf-slider__pager ewf-slider__pager--align-center"></div>
            <div class="ewf-slider__arrows"></div>
         </div>
         <!-- end .advanced-slider -->
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="1" data-customizer-section-string-id="services">
         <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}</style>
         <style type="text/css" media="all">#nos-services{margin-top:0px;padding-bottom:0px}</style>
         <div id="nos-services" class="section-services section ewf-section contrast ewf-section-visible ewf-valign--middle ewf-text-align--center ewf-section--title-top" >
            <div class="ewf-section__content">
               <div class="container-fluid ewf-padding-right--none ewf-padding-left--none">
                  <div class="row ">
                     <!-- Check if we have a title/subtitle -->
                     <div class="col-sm-12">
                        <div class="ewf-section-text">
                           <div class="headline">
                              <h3>Nos Services</h3>
                           </div>
                        </div>
                        <!--/.ewf-section-text-->
                     </div>
                     <!--/.col-->
                     <!-- // End Title Check -->
                     <!-- Check if we have values in our field repeater -->
                     <div class="col-sm-12 ewf-content__wrap">
                        <div class="row">
                           <div class="col-sm-4 ewf-item__spacing-lg">
                              <div class="services-item ewf-item__no-effect" style="background-color: ">
                                 <i class="ewf-icon fa fa-graduation-cap" style="color:#ea882e;font-size:36px;background-color:;border-color:;border-width:9px;border-radius:0px;padding:22px;"></i>
                                 <div class="ewf-like-h6">
                                    Formation en gestion durable														
                                 </div>
                                 <!--/.ewf-like-h6-->
                              </div>
                              <!--/.services-item-->
                           </div>
                           <!--/.col-sm-->
                           <div class="col-sm-4 ewf-item__spacing-lg">
                              <div class="services-item ewf-item__no-effect" style="background-color: ;background-color: ">
                                 <i class="ewf-icon fa fa-leaf" style="color:#ea882e;font-size:36px;background-color:;border-color:;border-width:9px;border-radius:0px;padding:22px;"></i>
                                 <div class="ewf-like-h6">
                                    Développement de projets en énergie verte														
                                 </div>
                                 <!--/.ewf-like-h6-->
                              </div>
                              <!--/.services-item-->
                           </div>
                           <!--/.col-sm-->
                           <div class="col-sm-4 ewf-item__spacing-lg">
                              <div class="services-item ewf-item__no-effect" style="background-color: ;background-color: ;background-color: ">
                                 <i class="ewf-icon fa fa-usd" style="color:#ea882e;font-size:36px;background-color:;border-color:;border-width:9px;border-radius:0px;padding:22px;"></i>
                                 <div class="ewf-like-h6">
                                    VSOFT : Analyse et réduction des coûts énergetique														
                                 </div>
                                 <!--/.ewf-like-h6-->
                              </div>
                              <!--/.services-item-->
                           </div>
                           <!--/.col-sm-->
                        </div>
                        <!--/.col-sm--->
                     </div>
                     <!--/.row-->
                  </div>
               </div>
            </div>
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="2" data-customizer-section-string-id="shortcodes">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style><style type="text/css" media="all">#shortcode-actu{background-color:#ffffff;}</style>		<div id="shortcode-actu" class="section-shortcodes section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--left ewf-section--title-top" >
      <div class="ewf-section__content">
      <div class="container">
      <div class="row">
      <div class="col-sm-12">
      <div class="headline"><h3>Actualités :</h3></div>														<div id="mtphr-dnt-335" class="mtphr-dnt mtphr-dnt-335 mtphr-dnt-default mtphr-dnt-scroll mtphr-dnt-scroll-left"><div class="mtphr-dnt-wrapper mtphr-dnt-clearfix"><div class="mtphr-dnt-tick-container"><div class="mtphr-dnt-tick-contents"><div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1076" target="_self">Formation : Contrôle des factures d&#8217;éléctricité dans le secteur public</a></div><div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1082" target="_self">Séminaire sur l&#8217;Efficacité énergétique du 17 au 22 octobre 2019 à Dakar (Sénégal)</a></div><div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1078" target="_self">Session facturation</a></div><div class="mtphr-dnt-tick mtphr-dnt-default-tick mtphr-dnt-clearfix "><a href="http://localhost/exergieafrique.dev/?p=1068" target="_self">Nouveautés et applications au service de la performance énergétique </a></div></div></div></div></div>						</div>
      </div>
      </div>
      </div>
      </div>
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="3" data-customizer-section-string-id="about">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style>		<div id="resume-pscofe" class="section-about section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--left ewf-section--title-right" >
      <div class="ewf-section__content">
      <div class="container">
      <div class="row row-flow-reverse">
      <div class="col-md-6">
      <div class="ewf-section-text">
      <div class="headline"><h3>VSOFT</h3></div>								<p>Ayez le juste prix de l’électricité débarrassée des anomalies, des pénalités, des fraudes et de l’arbitraire tout en tirant profit des incitatifs tarifaires que les gouvernements offrent pour soutenir la croissance. Les économies financières annuelles générées grâce au PSCOFE varient entre 5 et 20 % des dépenses initiales avec un temps de retour sur l’investissement inférieur à un an. </p>
      <a class="ewf-btn ewf-btn--huge" style="background-color:rgba(201, 91, 46, 1);color:#ffffff;border-color:#011769;border-radius:5px;" href="http://localhost/exergieafrique.dev/?page_id=74">Faites des économies d'éléctricité !</a>
      </div><!--/.ewf-section-text-->
      </div><!--/.col-md-->
      <div class="col-md-6">
      <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/logo-Complet-1.png" />
      </div><!--/.col-md--6-->
      </div><!--/.row-->
      </div><!--/.container class-->
      </div><!--/.ewf-section--content-->
      </div><!--/.attr-helper-->
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="4" data-customizer-section-string-id="counters">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style>		<div id="counters-100471" class="section-counters section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--center ewf-section--title-right" >
      <div class="ewf-section__content">
      <div class="container">
      <div class="row row-flow-reverse">
      <div class="col-sm-4">
      <div class="ewf-section-text">
      <div class="headline"><span>de retours sur investissement</span></div>							</div><!--ewf-section-text-->
      </div><!-- header class-->
      <div class="col-sm-8 ewf-content__wrap">
      </div><!-- content_class-->
      </div><!-- container_class-->
      </div><!--/.ewf-section__content-->
      </div><!--/.row-->
      </div><!-- attr generator-->
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="5" data-customizer-section-string-id="about">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style>		<div id="resume-pfeer" class="section-about section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--right ewf-section--title-left" >
      <div class="ewf-section__content">
      <div class="container">
      <div class="row ">
      <div class="col-md-6">
      <div class="ewf-section-text">
      <div class="headline"><h3>Formations</h3></div>								<p>Afin de repositionner l’énergie durable comme moyen efficace de rentabilité en entreprise, <b>Exergie Afrique</b> et son partenaire <b>CEGCI</b>, proposent un plan de renforcement des capacités intitulé « <b>Accès à l’énergie durable et ses avantages en Entreprise </b>».</p>
      <p>Ce plan de formations offre aux managers et gestionnaires ainsi qu’aux équipes techniques les connaissances requises pour la gestion quotidienne de leur entreprise, mais aussi pour discuter avec les professionnels en énergie lors de l’implantation des mesures d’énergie durable.</p>
      <a class="ewf-btn ewf-btn--huge" style="background-color:rgba(201, 91, 46, 1);color:#ffffff;border-color:#011769;border-radius:5px;" href="http://localhost/exergieafrique.dev/?page_id=44">Voir tous nos programmes !</a>
      </div><!--/.ewf-section-text-->
      </div><!--/.col-md-->
      <div class="col-md-6">
      <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/seye.jpg" />
      </div><!--/.col-md--6-->
      </div><!--/.row-->
      </div><!--/.container class-->
      </div><!--/.ewf-section--content-->
      </div><!--/.attr-helper-->
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="6" data-customizer-section-string-id="about">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style>		<div id="resume-pgeer" class="section-about section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--left ewf-section--title-right" >
      <div class="ewf-section__content">
      <div class="container">
      <div class="row row-flow-reverse">
      <div class="col-md-6">
      <div class="ewf-section-text">
      <div class="headline"><h3>Audit Énergétique</h3></div>								<p>La mise en place d’un programme d’efficacité énergétique et d’énergie renouvelable dans les entreprises offre des avantages pour les entreprises elles-mêmes ainsi que pour l'économie dans son ensemble. Elle offre aux compagnies des possibilités d’investissement permettant d’amortir aisément leur frais et de réaliser des profits substantiels dans des délais relativement courts.</p>
      <p>Exergie Afrique intervient dans la planification, l’exécution et le suivi de vos projet</p>
      <a class="ewf-btn ewf-btn--huge" style="background-color:rgba(201, 91, 46, 1);color:#ffffff;border-color:#011769;border-radius:5px;" href="http://localhost/exergieafrique.dev/?page_id=43">En savoir plus sur les Audits</a>
      </div><!--/.ewf-section-text-->
      </div><!--/.col-md-->
      <div class="col-md-6">
      <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/slide_helmet-resting-on-solar-panel_x500.jpg" />
      </div><!--/.col-md--6-->
      </div><!--/.row-->
      </div><!--/.container class-->
      </div><!--/.ewf-section--content-->
      </div><!--/.attr-helper-->
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="7">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style>		<div class="section-clientlist section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--center" >
      <div class="ewf-section__content">
      <div class="container">
      <div class="row ">
      <div class="col-sm-12">
      <div class="ewf-section-text">
      <div class="headline"><h3>Ils nous font confiance</h3>
      <span>Clients et Partenaires
                   </span></div>							</div><!--/.ewf-section--text-->
      </div><!--/.col-->
      <div class="col-sm-12 ewf-content__wrap">
      <div class="ewf-slider" data-slider-mode-fade="false"
         data-slider-speed="500"
         data-slider-autoplay="true"
         data-slides-shown="6"
         data-slides-scrolled="1"
         data-slider-loop="true"
         data-slider-enable-pager="false"
         data-slider-enable-controls="false">
      <ul class="ewf-slider__slides">
          <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/boad_logo.jpg" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="Pisam">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/logo_cie.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/pisam_logo.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/cli_pat_logo-sodeci.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/Flag_of_Togo.svg_.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/Coat_of_arms_of_Gabon.svg_.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/Asecna-logo.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/cli_pat_logo-ifdd2.jpg" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/unemaf_logo.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/socfin_sogb.jpeg" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/04/sococe_logo-1.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
                   <div class="col-sm-4">
                  <li class="ewf-partner ewf-item__no-effect" style="">
                      <a href="#">
                     <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/cli_pat_logo_envipur.png" alt="">
                      </a>
                  </li>
                  <!-- end .ewf-partner -->
                   </div>
                   <!--/.col-sm-->
      </ul><!-- end .ewf-partner-slider__slides -->
      <div class="ewf-slider__pager"></div>
      <div class="ewf-slider__arrows"></div>
      </div><!-- end .ewf-slider -->
      </div><!-- content class -->
      </div><!--row class-->
      </div><!-- container class -->
      </div><!--/.ewf-section--content-->
      </div><!--/ generate attr -->
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="8" data-customizer-section-string-id="cta">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style>		<div id="call-action" class="section-cta section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--center ewf-section--parallax" >
      <div class="ewf-section__content">
      <div class="container">
      <div class="row ">
      <div class="col-md-12">
      <div class="ewf-section-text">
      <div class="headline"><h3>Nous faisons rimer <b style="color:#6A9200">Écologie</b>, <b style="color:#9B2C00">Économies</b> et <b style="color:black">Profits</b> !</h3></div>							</div><!--/.ewf-section--text-->
      </div><!--/ header class-->
      <div class="col-md-12">
      <a class="ewf-btn ewf-btn--huge" style="background-color:rgba(249, 250, 252, 0.4);color:rgba(0, 0, 0, 1);border-color:rgba(249, 250, 252, 0.4);border-radius:0px;" href="">Pour plus de Renseignements,</a>
      <a class="ewf-btn ewf-btn--huge" style="background-color:rgba(255, 255, 255, 0.5);color:rgba(0, 0, 0, 1);border-color:rgba(255, 255, 255, 0.5);border-radius:0px;" href="">Écrivez nous !</a>
      </div><!-- content class -->
      </div><!--/.row-->
      </div><!-- container class -->
      </div><!-- ewf-section__content-->
      </div><!-- attr generator-->
      </section>
      <section data-customizer-section-id="exergie_repeatable_section" data-section="9" data-customizer-section-string-id="shortcodes">
      <style type="text/css">[data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      [data-section="1"] h1,[data-section="1"] h2,[data-section="1"] h3,[data-section="1"] h4,[data-section="1"] h5,[data-section="1"] h6,[data-section="1"] .headline span:not(.dashicons),[data-section="1"] .headline h3,[data-section="1"] .services-item span:not(.dashicons){ color: #C95A2E}
      [data-section="2"] h1,[data-section="2"] h2,[data-section="2"] h3,[data-section="2"] h4,[data-section="2"] h5,[data-section="2"] h6,[data-section="2"] .headline span{ color: #C95A2E}
      </style><style type="text/css" media="all">#shortcodes-470919{background-image:url(http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/énergies-renouvelables.jpg);background-position:center;background-size:cover;background-repeat:no-repeat}</style>		<div id="shortcodes-470919" class="section-shortcodes section ewf-section ewf-section-visible ewf-valign--middle ewf-text-align--center" >
      <div class="ewf-section__overlay-color"  style="background-color:rgba(147, 189, 42, 0.5);" ></div>		<div class="ewf-section__content">
      <div class="container">
      <div class="row">
      <div class="col-sm-12">
      <div class="headline"></div>														<div role="form" class="wpcf7" id="wpcf7-f289-o1" lang="en-US" dir="ltr">
      <div class="screen-reader-response"></div>
      <form action="/exergieafrique.dev/#wpcf7-f289-o1" method="post" class="wpcf7-form" novalidate="novalidate">
      <div style="display: none;">
      <input type="hidden" name="_wpcf7" value="289" />
      <input type="hidden" name="_wpcf7_version" value="5.1.3" />
      <input type="hidden" name="_wpcf7_locale" value="en_US" />
      <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f289-o1" />
      <input type="hidden" name="_wpcf7_container_post" value="0" />
      </div>
      <p><label> Nom (requis)<br />
      <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </label></p>
      <p><label> Prénom (requis)<br />
      <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </label></p>
      <p><label> Téléphone (requis)<br />
      <span class="wpcf7-form-control-wrap tel-544"><input type="tel" name="tel-544" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" /></span></label></p>
      <p><label> Mail (requis)<br />
      <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </label></p>
      <p><label> Sujet<br />
      <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </label></p>
      <p><label> Votre Message<br />
      <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span> </label></p>
      <p><input type="submit" value="Envoyez" class="wpcf7-form-control wpcf7-submit" /></p>
      <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>						</div>
      </div>
      </div>
      </div>
      </div>
      </section>
      <div id="footer">
      <div id="footer-bottom" class="row footer-sub">
      <!-- /// FOOTER-BOTTOM  ////////////////////////////////////////////////////////////////////////////////////////////// -->
      <div class="container">
      <div class="row">
      <div id="footer-bottom-widget-area-1" class="col-sm-6 ol-xs-12">
      <div class="pull-rigth">
      Theme by: <a href="https://home.tiko.ci/">Tiko Themes</a>. All rights reserved &copy 2019.			 </div>
      </div><!-- end .col -->
      <div id="footer-bottom-widget-area-2" class="col-sm-6 col-xs-12">
      <div id="custom_html-7" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget">    <div class="footer-social-icons">
      <a target="_blank" href="http://www.twitter.com" title="Twitter">
      <i class="fa fa-twitter">
      </i>
      </a>
      <a target="_blank" href="http://www.facebook.com" title="Facebook">
      <i class="fa fa-facebook">
      </i>
      </a>
      <a target="_blank" href="https://www.linkedin.com/" title="Linkedin ">
      <i class="fa fa-linkedin">
      </i>
      </a>
      </div>
      </div></div><ul id="menu-menu-de-pied-de-page" class="nav"><li id="menu-item-897" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-897"><a href="#">Protection des données</a></li>
      <li id="menu-item-898" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-898"><a href="#">Carte du site</a></li>
      </ul>		</div><!-- end .col -->
      </div><!-- end .row -->
      </div><!-- end .container -->
      <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      </div><!-- end #footer-bottom -->
      </div>
      </div>
      <a id="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
      <script>
         ( function ( body ) {
         	'use strict';
         	body.className = body.className.replace( /\btribe-no-js\b/, 'tribe-js' );
         } )( document.body );
      </script>
      <script> /* <![CDATA[ */var tribe_l10n_datatables = {"aria":{"sort_ascending":": activate to sort column ascending","sort_descending":": activate to sort column descending"},"length_menu":"Show _MENU_ entries","empty_table":"No data available in table","info":"Showing _START_ to _END_ of _TOTAL_ entries","info_empty":"Showing 0 to 0 of 0 entries","info_filtered":"(filtered from _MAX_ total entries)","zero_records":"No matching records found","search":"Search:","all_selected_text":"All items on this page were selected. ","select_all_link":"Select all pages","clear_selection":"Clear Selection.","pagination":{"all":"All","next":"Next","previous":"Previous"},"select":{"rows":{"0":"","_":": Selected %d rows","1":": Selected 1 row"}},"datepicker":{"dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesMin":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Prev","currentText":"Today","closeText":"Done","today":"Today","clear":"Clear"}};var tribe_system_info = {"sysinfo_optin_nonce":"3f72be3897","clipboard_btn_text":"Copy to clipboard","clipboard_copied_text":"System info copied","clipboard_fail_text":"Press \"Cmd + C\" to copy"};/* ]]> */ </script>
      <link rel='stylesheet' id='slick-css'  href='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/vendors/slick/slick.css?ver=5.1.1' type='text/css' media='all' />
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/admin-bar.min.js?ver=5.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var wpcf7 = {"apiSettings":{"root":"http:\/\/localhost\/exergieafrique.dev\/index.php?rest_route=\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/jquery.touchSwipe.min.js?ver=2.2.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/jquery.easing.js?ver=1.4.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/imagesloaded.pkgd.min.js?ver=4.1.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var mtphr_dnt_vars = {"is_rtl":""};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/plugins/ditty-news-ticker/legacy/static/js/ditty-news-ticker.min.js?ver=1559147953'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/vendors/viewport/viewport.js?ver=1.0.7'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/vendors/plyr/plyr.js?ver=1.0.7'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/js/exergie.js?ver=1.0.7'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/js/tiko-func.js?ver=1.0.7'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/js/main.js?ver=1.0.7'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-includes/js/wp-embed.min.js?ver=5.1.1'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/vendors/slick/slick.js?ver=1.0.7'></script>
      <script type='text/javascript' src='http://localhost/exergieafrique.dev/wp-content/themes/exergie-child/assets/vendors/odometer/odometer.min.js?ver=1.0.7'></script>
      <script>
         jQuery( document ).ready( function($) {
         				$( '#mtphr-dnt-335' ).ditty_news_ticker({
         		id : '335',
         		type : 'scroll',
         		scroll_direction : 'left',
         		scroll_speed : 6,
         		scroll_pause : 0,
         		scroll_spacing : 200,
         		scroll_init : 0,
         		rotate_type : 'fade',
         		auto_rotate : 1,
         		rotate_delay : 7,
         		rotate_pause : 0,
         		rotate_speed : 10,
         		rotate_ease : 'easeInOutQuint',
         		nav_reverse : 0,
         		disable_touchswipe : 0,
         		offset : 20,
         		after_load : function( $ticker ) {
         								},
         		before_change : function( $ticker ) {
         								},
         		after_change : function( $ticker ) {
         								}
         	});
          			});
      </script>
      <!--[if lte IE 8]>
      <script type="text/javascript">
         document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
      </script>
      <![endif]-->
      <!--[if gte IE 9]><!-->
      <script type="text/javascript">
         (function() {
         	var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');
         
         			request = true;
         
         	b[c] = b[c].replace( rcs, ' ' );
         	// The customizer requires postMessage and CORS (if the site is cross domain)
         	b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
         }());
      </script>
      <!--<![endif]-->
      <div id="wpadminbar" class="nojq nojs">
         <a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
         <div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar">
            <ul id='wp-admin-bar-root-default' class="ab-top-menu">
               <li id='wp-admin-bar-wp-logo' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/about.php'><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-wp-logo-default' class="ab-submenu">
                        <li id='wp-admin-bar-about'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/about.php'>About WordPress</a></li>
                     </ul>
                     <ul id='wp-admin-bar-wp-logo-external' class="ab-sub-secondary ab-submenu">
                        <li id='wp-admin-bar-wporg'><a class='ab-item' href='https://wordpress.org/'>WordPress.org</a></li>
                        <li id='wp-admin-bar-documentation'><a class='ab-item' href='https://codex.wordpress.org/'>Documentation</a></li>
                        <li id='wp-admin-bar-support-forums'><a class='ab-item' href='https://wordpress.org/support/'>Support Forums</a></li>
                        <li id='wp-admin-bar-feedback'><a class='ab-item' href='https://wordpress.org/support/forum/requests-and-feedback'>Feedback</a></li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-site-name' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/'>Exergie Afrique</a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-site-name-default' class="ab-submenu">
                        <li id='wp-admin-bar-dashboard'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/'>Dashboard</a></li>
                     </ul>
                     <ul id='wp-admin-bar-appearance' class="ab-submenu">
                        <li id='wp-admin-bar-themes'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/themes.php'>Themes</a></li>
                        <li id='wp-admin-bar-widgets'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/widgets.php'>Widgets</a></li>
                        <li id='wp-admin-bar-menus'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/nav-menus.php'>Menus</a></li>
                        <li id='wp-admin-bar-header' class="hide-if-customize"><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/themes.php?page=custom-header'>Header</a></li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-customize' class="hide-if-no-customize"><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/customize.php?url=http%3A%2F%2Flocalhost%2Fexergieafrique.dev%2F'>Customize</a></li>
               <li id='wp-admin-bar-updates'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/update-core.php' title='5 Plugin Updates, Translation Updates'><span class="ab-icon"></span><span class="ab-label">6</span><span class="screen-reader-text">5 Plugin Updates, Translation Updates</span></a></li>
               <li id='wp-admin-bar-comments'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit-comments.php'><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a></li>
               <li id='wp-admin-bar-bar_wpbc' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc'>Booking Calendar</a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-bar_wpbc-default' class="ab-submenu">
                        <li id='wp-admin-bar-bar_wpbc_calendar_overview'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc&#038;view_mode=vm_calendar'>Calendar Overview</a></li>
                        <li id='wp-admin-bar-bar_wpbc_booking_listing'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc&#038;view_mode=vm_listing'>Booking Listing</a></li>
                        <li id='wp-admin-bar-bar_wpbc_new'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-new'>Add booking</a></li>
                        <li id='wp-admin-bar-bar_wpbc_settings' class="menupop">
                           <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings'>Settings</a>
                           <div class="ab-sub-wrapper">
                              <ul id='wp-admin-bar-bar_wpbc_settings-default' class="ab-submenu">
                                 <li id='wp-admin-bar-bar_wpbc_settings_form'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings&#038;tab=form'>Form</a></li>
                                 <li id='wp-admin-bar-bar_wpbc_settings_email'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings&#038;tab=email'>Emails</a></li>
                                 <li id='wp-admin-bar-bar_wpbc_settings_sync'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=wpbc-settings&#038;tab=sync'>Sync</a></li>
                              </ul>
                           </div>
                        </li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-new-content' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/post-new.php'><span class="ab-icon"></span><span class="ab-label">New</span></a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-new-content-default' class="ab-submenu">
                        <li id='wp-admin-bar-new-post'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php'>Post</a></li>
                        <li id='wp-admin-bar-new-media'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/media-new.php'>Media</a></li>
                        <li id='wp-admin-bar-new-page'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=page'>Page</a></li>
                        <li id='wp-admin-bar-new-ditty_news_ticker'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=ditty_news_ticker'>News Ticker</a></li>
                        <li id='wp-admin-bar-new-tribe_events'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=tribe_events'>Event</a></li>
                        <li id='wp-admin-bar-new-user'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/user-new.php'>User</a></li>
                     </ul>
                  </div>
               </li>
               <li id='wp-admin-bar-edit'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post.php?post=258&#038;action=edit'>Edit Page</a></li>
               <li id='wp-admin-bar-maintenance_options'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/admin.php?page=maintenance' title='Maintenance is Off'>Maintenance is Off</a></li>
               <li id='wp-admin-bar-tribe-events' class="menupop">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev?post_type=tribe_events'><span class="ab-icon dashicons-before dashicons-calendar"></span>Events</a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-tribe-events-group' class="ab-submenu">
                        <li id='wp-admin-bar-tribe-events-view-calendar'><a class='ab-item' href='http://localhost/exergieafrique.dev?post_type=tribe_events'>View Calendar</a></li>
                        <li id='wp-admin-bar-tribe-events-add-event'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/post-new.php?post_type=tribe_events'>Add Event</a></li>
                        <li id='wp-admin-bar-tribe-events-edit-events'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?post_type=tribe_events'>Edit Events</a></li>
                     </ul>
                     <div id="wp-admin-bar-tribe-events-add-ons-group-container" class="ab-group-container">
                        <ul id='wp-admin-bar-tribe-events-import-group' class="ab-submenu">
                           <li id='wp-admin-bar-tribe-events-import' class="menupop">
                              <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/edit.php?post_type=tribe_events&#038;page=aggregator'>Import</a>
                              <div class="ab-sub-wrapper">
                                 <ul id='wp-admin-bar-tribe-events-import-default' class="ab-submenu">
                                    <li id='wp-admin-bar-tribe-aggregator-import-csv'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?page=aggregator&#038;post_type=tribe_events&#038;ea-origin=csv'>CSV File</a></li>
                                 </ul>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <ul id='wp-admin-bar-tribe-events-settings-group' class="ab-submenu">
                        <li id='wp-admin-bar-tribe-events-settings'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?page=tribe-common&#038;post_type=tribe_events'>Settings</a></li>
                        <li id='wp-admin-bar-tribe-events-help'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?post_type=tribe_events&#038;page=tribe-help'>Help</a></li>
                        <li id='wp-admin-bar-tribe-events-app-shop'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/edit.php?page=tribe-app-shop&#038;post_type=tribe_events'>Event Add-Ons</a></li>
                     </ul>
                  </div>
               </li>
            </ul>
            <ul id='wp-admin-bar-top-secondary' class="ab-top-secondary ab-top-menu">
               <li id='wp-admin-bar-search' class="admin-bar-search">
                  <div class="ab-item ab-empty-item" tabindex="-1">
                     <form action="http://localhost/exergieafrique.dev/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form>
                  </div>
               </li>
               <li id='wp-admin-bar-my-account' class="menupop with-avatar">
                  <a class='ab-item' aria-haspopup="true" href='http://localhost/exergieafrique.dev/wp-admin/profile.php'>Howdy, <span class="display-name">Anémone Kouadio</span><img alt='' src='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=26&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=52&#038;d=mm&#038;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a>
                  <div class="ab-sub-wrapper">
                     <ul id='wp-admin-bar-user-actions' class="ab-submenu">
                        <li id='wp-admin-bar-user-info'><a class='ab-item' tabindex="-1" href='http://localhost/exergieafrique.dev/wp-admin/profile.php'><img alt='' src='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=64&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/b93b7de1da81571d43a6485c0167591b?s=128&#038;d=mm&#038;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>Anémone Kouadio</span><span class='username'>anemone</span></a></li>
                        <li id='wp-admin-bar-edit-profile'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-admin/profile.php'>Edit My Profile</a></li>
                        <li id='wp-admin-bar-logout'><a class='ab-item' href='http://localhost/exergieafrique.dev/wp-login.php?action=logout&#038;_wpnonce=094fea142a'>Log Out</a></li>
                     </ul>
                  </div>
               </li>
            </ul>
         </div>
         <a class="screen-reader-shortcut" href="http://localhost/exergieafrique.dev/wp-login.php?action=logout&#038;_wpnonce=094fea142a">Log Out</a>
      </div>
   </body>
</html>

