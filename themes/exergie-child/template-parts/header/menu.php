<?php
/**
 * Template part for displaying the menu
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Exergie
 */

?>

<div class="exergie-menu-icon">
    <div class="exergie-navicon"></div>
</div>

<?php
$header_bg = get_theme_mod( 'exergie_header_background', false );
$class     = ! $header_bg ? 'exergie-menu fixed' : 'exergie-menu fixed header-background';

/* wp_nav_menu( array(
 *     'menu'           => 'social',
 *     'theme_location' => 'social',
 *     'container'      => '',
 *     'menu_id'        => 'social',
 *     'menu_class'     => $class,
 * ) ); */

dynamic_sidebar( 'header-sidebar-5' );

wp_nav_menu( array(
    'menu'           => 'primary',
    'theme_location' => 'primary',
    'container'      => '',
    'menu_id'        => 'menu',
    'menu_class'     => $class,
) );
?>
