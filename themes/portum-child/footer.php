<div id="footer">
      <div id="footer-widgets">
      <div class="container">
      <div class="row">
      <div id="footer-widget-area-1" class="col-sm-5">
      <div id="custom_html-4" class="widget_text widget widget_custom_html"><h5 class="widget-title">Contacts</h5><div class="textwidget custom-html-widget"><div class="address-box">
      <div class="address-contact">
      <span class="fa-stack"><i class="fa fa-circle fa-stack-2x"> </i> <i class="fa fa-map-marker fa-stack-1x fa-inverse"> </i> </span>
      <div class="address-content">321, rue du Lac, Abidjan 2 Plateaux-Versants 2 <br> 27 BP 561 Abidjan 27<br>Côte d'Ivoire<br>
      </div>
      </div>
      <div class="address-contact">
      <span class="fa-stack"> <i class="fa fa-circle fa-stack-2x"> </i><i class="fa fa-phone fa-stack-1x fa-inverse"> </i> </span>
      <div class="address-content">
      <ul>
      <li>00 (225) 22 42 77 28</li>
      <li>00 (225) 08 17 19 19</li>
      <li>00 (225) 76 76 58 76</li>
      </ul>
      </div>
      </div>
      <div class="address-contact">
      <span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i></span>
      <div class="address-content">
      <a href="#"> vsoft@localhost/exergieafrique.dev</a>
      </div>
      </div>
      </div>
      </div></div><div id="text-3" class="widget widget_text">       <div class="textwidget"><pre class="" contenteditable="true"><code><div class="su-gmap su-responsive-media-yes"><iframe src="//maps.google.com/maps?q=321%2C+rue+du+Lac%2C+Abidjan&amp;output=embed"></iframe></div></code></pre>
      </div>
      </div>                     </div>
      <div id="footer-widget-area-2" class="col-sm-7">
      <div id="text-9" class="widget widget_text"><h5 class="widget-title">Demande d&#8217;information</h5>       <div class="textwidget"><div role="form" class="wpcf7" id="wpcf7-f289-o2" lang="en-US" dir="ltr">
      <div class="screen-reader-response"></div>
      <form action="/#wpcf7-f289-o2" method="post" class="wpcf7-form" novalidate="novalidate">
      <div style="display: none;">
      <input type="hidden" name="_wpcf7" value="289" />
      <input type="hidden" name="_wpcf7_version" value="5.1.1" />
      <input type="hidden" name="_wpcf7_locale" value="en_US" />
      <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f289-o2" />
      <input type="hidden" name="_wpcf7_container_post" value="0" />
      <input type="hidden" name="g-recaptcha-response" value="" />
      </div>
      <p><label> Votre Nom (requis)<br />
      <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span> </label></p>
      <p><label> Votre Mail (requis)<br />
      <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </label></p>
      <p><label> Sujet<br />
      <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span> </label></p>
      <p><label> Votre Message<br />
      <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
      <p><input type="submit" value="Envoyez" class="wpcf7-form-control wpcf7-submit" /></p>
      <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
      </div>
      </div>
      <div id="social-footer" class="pull-right"> <?php echo do_shortcode('[aps-social id="3"]')?> </div>                  </div>
      </div><!--.row-->
      </div>
      </div><!--/#footer-widgets-->
      <div id="footer-bottom" class="row footer-sub">
      <!-- /// FOOTER-BOTTOM  ////////////////////////////////////////////////////////////////////////////////////////////// -->
      <div class="container">
       <div class="row">
            <div id="footer-bottom-widget-area-1" class="col-sm-6 ol-xs-12">
            <img src="http://localhost/exergieafrique.dev/wp-content/uploads/2019/05/exergie-afrique.png" class="img-responsive" alt="logo-exergieafrique">
            </div><!-- end .col -->
            <div id="footer-bottom-widget-area-2" class="col-sm-6 col-xs-12">
            <ul id="menu-nos-formations" class="nav">
               <div class="row">
           
               </div>
            </ul>             
</div><!-- end .col -->
      </div><!-- end .row -->
      <hr></hr>
      <div class="row"> 
            <div id="footer-bottom-widget-area-3" class="col-sm-12">
              <p> Exergie Afrique. All rights reserved &copy 2019.</p>  
            </div>
      </div>
      </div><!-- end .container -->
      <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      </div><!-- end #footer-bottom -->