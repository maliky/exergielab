<?php get_header();
$layout = Exergie_Helper::get_layout( 'exergie_blog_layout' );
?>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<h2><?php the_title();?></h2>
			<hr></hr>
			<?php
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content/content', 'single' );
					endwhile;
				else :
					get_template_part( 'template-parts/content/content', 'none' );
				endif;

			the_posts_pagination(
		    array(
			'prev_text' => '<span class="fa fa-angle-left"></span> ' . esc_html__( 'Previous', 'exergie' ),
			'next_text' => esc_html__( 'Next', 'exergie' ) . ' <span class="fa fa-angle-right"></span>',
		    )
		);
		?>
		</div>
		<div class="col-sm-5 feature">
			<?php if ( 'left-sidebar' === $layout['type'] && is_active_sidebar( 'sidebar' ) ) : ?>
				
		    <?php dynamic_sidebar( 'sidebar' ); ?>
				
	    	<?php endif; ?>
<p>Cet article est référencé dans : <?php the_category(', '); ?></p> 
		</div>
		
	</div>
	
</div>
<?php get_footer();
?>